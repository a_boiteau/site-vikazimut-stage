# Contribuer au développement du site Vikazimut

## Installation de symfony pour le développement

### installer php et Symfony

- Installer php >=7.4 et tout ce qu'il faut comme extensions
- Installer composer
- Installer symfony
  
### Cloner le projet sur le GitLab

Une fois le projet cloné, il faut :
- faire `composer install` pour installer tous les bundles. 
- installer une base
   ```
   symfony server:start --dir=site-vikazimut/public 
   ```

### Installer la base de données mysql

- Installer mysql
- Ajouter le fichier .env.local
- Créer un compte dans la base de données
- Ajouter ce compte dans le fichier .env.local
- Ajouter le dossier config/secret
- Installer les fichiers des essais
	- Ajouter un exemple de base de données :  mysql < base.sql
	- Ajouter un exemple de dossier `upload` à la racine du projet

## Reinitialiser les mots de passe

- Éditer le fichier `Model/CreateUser.php`.
- Commenter le code de la fonction `changePassword`.
- Remplacer par `return "toto"` qui devient le nouveau mot de passe.
- Sur le serveur; demander la réinitialisation du mot de passe.
- Essayer ensuite de se connecter avec le mot ce passe "toto".

## Personnalisation de Bootstrap

Le site suivant permet de personnaliser Bootstrap, puis de récupérer les css modifiés pour les mettre dans le fichier `theme.css` :

[https://themestr.app/](https://themestr.app/)

## Version en production

### Installation

Vérifier la taille maximale des fichiers téléversables (eg. dans php.ini ou configuration apache)

### Mise à jour de la version de Symfony

Le fichier `composer.json` référence les paquets utilisés par le projet avec leur numéro de version.
Pour changer la version de Symfony, il faut mettre à jour la version des paquets Symfony, puis lancer la commande :

```
php7.4-cli composer.phar update –no-dev --with-all-dependencies
```

### Déploiement en production

#### Version avec Easy Deploy

    php bin/console deploy

#### Version à la main

Vider le cache avant de lancer une nouvelle version.
    
    php bin/console cache:clear --env=prod --no-debug
    
Mettre à jour la base de données

    php bin/console doctrine:migrations:migrate

### Lancement du serveur

    symfony server:start --document-root=site-vikazimute
