<?php
///*
//namespace App\Tests;
//
//use PHPUnit\Framework\TestCase;
//use App\Entity\Course;
//
/*//class CourseTest extends TestCase
//{
//    public function produceXmlMap(){
//        return '<?xml version="1.0" encoding="UTF-8" */?><!--
//       dd_course <CourseData >
//          <RaceCourseData>
//            <Control>
//              <Id>S1</Id>
//              <Position lng="-0.538659" lat="49.285667" />
//              <MapPosition x="116.0" y="65.8" unit="mm" />
//            </Control>
//            <Control>
//              <Id>31</Id>
//              <Position lng="-0.539075" lat="49.286048" />
//              <MapPosition x="110.3" y="74.6" unit="mm" />
//            </Control>
//            <Control>
//              <Id>F1</Id>
//              <Position lng="-0.538668" lat="49.285673" />
//              <MapPosition x="115.9" y="66.0" unit="mm" />
//            </Control>
//            <Course>
//              <CourseControl type="Start">
//                <Control>S1</Control>
//              </CourseControl>
//              <CourseControl type="Control">
//                <Control>31</Control>
//              </CourseControl>
//              <CourseControl type="Finish">
//                <Control>F1</Control>
//              </CourseControl>
//            </Course>
//          </RaceCourseData>
//        </CourseData>';
//    }-->
//
<!--//    public function produceXmlStartEnd(){
//        return '<?/*xml version="1.0" encoding="UTF-8" */?>
//        <CourseData >
//          <RaceCourseData>
//            <Map>
//              <Scale>5000</Scale>
//              <MapPositionTopLeft x="-9449.8" y="1581.8" unit="mm"/>
//              <MapPositionBottomRight x="2991.2" y="-9999.7" unit="mm"/>
//            </Map>
//            <Control>
//              <Id>31</Id>
//              <Position lng="-0.539075" lat="49.286048" />
//              <MapPosition x="110.3" y="74.6" unit="mm" />
//            </Control>
//            <Course>
//              <CourseControl type="Control">
//                <Control>31</Control>
//              </CourseControl>
//            </Course>
//          </RaceCourseData>
//        </CourseData>';
//    }-->
//
<!--//    public function produceXmlControl(){
//        return '<?/*xml version="1.0" encoding="UTF-8" */?>
//        <CourseData >
//          <RaceCourseData>
//            <Map>
//              <Scale>5000</Scale>
//              <MapPositionTopLeft x="-9449.8" y="1581.8" unit="mm"/>
//              <MapPositionBottomRight x="2991.2" y="-9999.7" unit="mm"/>
//            </Map>
//            <Control>
//              <Id>S1</Id>
//              <Position lng="-0.538659" lat="49.285667" />
//              <MapPosition x="116.0" y="65.8" unit="mm" />
//            </Control>
//            <Control>
//              <Id>31</Id>
//              <Position lng="-0.539075" lat="49.286048" />
//              <MapPosition x="110.3" y="74.6" unit="mm" />
//            </Control>
//            <Control>
//              <Id>F1</Id>
//              <Position lng="-0.538668" lat="49.285673" />
//              <MapPosition x="115.9" y="66.0" unit="mm" />
//            </Control>
//            <Course>
//              <CourseControl type="Start">
//                <Control>S1</Control>
//              </CourseControl>
//              <CourseControl type="Control">
//                <Control>32</Control>
//              </CourseControl>
//              <CourseControl type="Finish">
//                <Control>F1</Control>
//              </CourseControl>
//            </Course>
//          </RaceCourseData>
//        </CourseData>';
//    }
-->
<!--//    public function produceXmlValid(){
//        return '<?/*xml version="1.0" encoding="UTF-8" */?>
//        <CourseData >
//          <RaceCourseData>
//            <Map>
//              <Scale>5000</Scale>
//              <MapPositionTopLeft x="-9449.8" y="1581.8" unit="mm"/>
//              <MapPositionBottomRight x="2991.2" y="-9999.7" unit="mm"/>
//            </Map>
//            <Control>
//              <Id>S1</Id>
//              <Position lng="-0.538659" lat="49.285667" />
//              <MapPosition x="116.0" y="65.8" unit="mm" />
//            </Control>
//            <Control>
//              <Id>31</Id>
//              <Position lng="-0.539075" lat="49.286048" />
//              <MapPosition x="110.3" y="74.6" unit="mm" />
//            </Control>
//            <Control>
//              <Id>F1</Id>
//              <Position lng="-0.538668" lat="49.285673" />
//              <MapPosition x="115.9" y="66.0" unit="mm" />
//            </Control>
//
//            <Course>
//              <Name>1H</Name>
//              <Length>2460</Length>
//              <Climb>0</Climb>
//              <CourseControl type="Start">
//                <Control>S1</Control>
//              </CourseControl>
//              <CourseControl type="Control">
//                <Control>31</Control>
//                <LegLength>52</LegLength>
//              </CourseControl>
//              <CourseControl type="Finish">
//                <Control>F1</Control>
//                <LegLength>414</LegLength>
//              </CourseControl>
//            </Course>
//
//          </RaceCourseData>
//        </CourseData>';
//    }-->
//
//    public function testXml(){
//        $map = $this->produceXmlMap();
//        $startEnd = $this->produceXmlStartEnd();
//        $control = $this->produceXmlControl();
//        $valid = $this->produceXmlValid();
//
//        $course = new Course();
//
//        $course->setXml("Ceci n'est pas un xml");
//        $this->assertFalse($course->isXmlValid());
//        $this->assertEquals("Xml non valide", $course->getError());
//
//        $course->setXml($map);
//        $this->assertFalse($course->isXmlValid());
//        $this->assertEquals("Positionnement sur la carte non valide", $course->getError());
//
//        $course->setXml($startEnd);
//        $this->assertFalse($course->isXmlValid());
//        $this->assertEquals("Balise de départ ou de fin manquante", $course->getError());
//
//        $course->setXml($control);
//        $this->assertFalse($course->isXmlValid());
//        $this->assertEquals("Une ou plusieurs balises sont invalides", $course->getError());
//
//        $course->setXml($valid);
//        $this->assertTrue($course->isXmlValid());
//        $this->assertEquals("", $course->getError());
//    }
<!--//
//    public function produceKmlCoord(){
//        return '<?/*xml version="1.0" encoding="UTF-8"*/?>
//        <kml xmlns="http://www.opengis.net/kml/2.2">
//        <Folder>
//            <GroundOverlay>
//                <LatLonBox>
//                    <north>49.289583330</north>
//                    <west>-0.558151851</west>
//                    <rotation>2.581489168</rotation>
//                </LatLonBox>
//            </GroundOverlay>
//        </Folder>
//        </kml>';
//    }
//-->
<!--//    public function produceKmlValid(){
//        return '<?/*xml version="1.0" encoding="UTF-8"*/?>
//        <kml xmlns="http://www.opengis.net/kml/2.2">
//        <Folder>
//            <GroundOverlay>
//                <LatLonBox>
//                    <north>49.289583330</north>
//                    <south>49.276247370</south>
//                    <east>-0.529324163</east>
//                    <west>-0.558151851</west>
//                    <rotation>2.581489168</rotation>
//                </LatLonBox>
//            </GroundOverlay>
//        </Folder>
//        </kml>';
//    }
-->//
//    public function testKml(){
//        $coord = $this->produceKmlCoord();
//        $valid = $this->produceKmlValid();
//
//        $course = new Course();
//
//        $course->setKml("Ceci n'est pas un kml");
//        $this->assertFalse($course->isKmlValid());
//        $this->assertEquals("Kml non valide", $course->getError());
//
//        $course->setKml($coord);
//        $this->assertFalse($course->isKmlValid());
//        $this->assertEquals("Coordonnée non valide", $course->getError());
//
//        $course->setKml($valid);
//        $this->assertTrue($course->isKmlValid());
//        $this->assertEquals("", $course->getError());
//    }
//
//    public function xmlKmlProvider(){
//        $map = $this->produceXmlMap();
//        $validXml = $this->produceXmlValid();
//        $coord = $this->produceKmlCoord();
//        $validKml = $this->produceKmlValid();
//
//        return [
//            [$map, $coord, false],
//            [$map, $validKml, false],
//            [$validXml, $coord, false],
//            [$validXml, $validKml, true]
//        ];
//    }
//
//    /**
//     * @require testXml
//     * @require testKml
//     * @dataProvider xmlKmlProvider
//     */
//    public function testIsValid($xml, $kml, $expected){
//        $course = new Course();
//
//        $file = "upload/test.png";
//        $handle = fopen($file, 'a'); //create file
//
//        $course->setImage($file);
//        $course->setXml($xml);
//        $course->setKml($kml);
//        $this->assertEquals($course->isValid(), $expected);
//
//        fclose($handle);
//        unlink($file);
//    }
//}*/
