<?php

namespace App\Tests;

use App\Entity\Event;
use App\Entity\Participant;
use App\Entity\ParticipantMakeEventCourse;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EventTest extends KernelTestCase
{
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRemove()
    {
        $event = $this->entityManager->getRepository(Event::class)->findAll()[0];
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(Event::class)->findAll());
        $event->remove($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(Event::class)->findAll()) + 1);
    }

    public function testUpdateCheckNbOT()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $theParticipant = null;
        foreach ($theEvent->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $theParticipant = $participant;
                break;
            }
        }
        $theEventCourse = null;
        foreach ($theEvent->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $theEventCourse = $eventCourse;
                break;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $theEventCourse, "participant" => $theParticipant));

        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(3, $participantMakeEventCourse->getNbOverTimePenalty());
    }

    public function testUpdateCheckNbPM()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $theParticipant = null;
        foreach ($theEvent->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $theParticipant = $participant;
                break;
            }
        }
        $theEventCourse = null;
        foreach ($theEvent->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $theEventCourse = $eventCourse;
                break;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $theEventCourse, "participant" => $theParticipant));

        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(1, $participantMakeEventCourse->getNbMissingPunchPenalty());
    }

    public function testUpdateChampionshipCheckScoreFirst()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $theParticipant = null;
        foreach ($theEvent->getParticipants() as $participant) {
            if ($participant->getNickName() === "Suliac") {
                $theParticipant = $participant;
                break;
            }
        }
        $theEventCourse = null;
        foreach ($theEvent->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $theEventCourse = $eventCourse;
                break;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $theEventCourse, "participant" => $theParticipant));

        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(1000, $participantMakeEventCourse->getScore());
    }

    public function testUpdateChampionshipCheckScoreSecond()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $theParticipant = null;
        foreach ($theEvent->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $theParticipant = $participant;
                break;
            }
        }
        $theEventCourse = null;
        foreach ($theEvent->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $theEventCourse = $eventCourse;
                break;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $theEventCourse, "participant" => $theParticipant));

        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(741, $participantMakeEventCourse->getScore());
    }

    public function testGetCSV()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $csv = $theEvent->getCSV($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $expectedCSV = "nickname,totalScore,Score,Time,(PM),(OT),S1,36,31,32,33,43,49,34,35,37,38,45,46,47,39,48,40,41,42,F1,Score,Time,(PM),(OT),S1,31,32,33,34,35,36,37,38,39,40,41,42,F1\nSuliac,2000,1000,2417000,0,1,0,248000,378000,532000,692000,797000,921000,1153000,1255000,1373000,1563000,1773000,1827000,1891000,1977000,2065000,2163000,2297000,2377000,2417000,1000,1719000,0,0,0,92000,236000,368000,567000,726000,884000,1098000,1218000,1338000,1421000,1534000,1623000,1685000\nEric,741,741,2540000,1,3,0,141000,245000,391000,548000,653000,777000,1040000,1151000,1295000,1513000,1738000,1791000,1904000,2068000,2154000,2261000,2400000,2493000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        $this->assertSame($csv, $expectedCSV);
    }

    public function testExtractRankingChampionShipRank()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(0, $ranking["rank"][0]);
    }

    public function testExtractRankingChampionShipTotalScore()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(2000, $ranking["totalScore"][0]);
        $this->assertSame(741, $ranking["totalScore"][1]);
    }

    public function testExtractRankingChampionShipProgress()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Championship") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(2, $ranking["progress"][0]);
        $this->assertSame(1, $ranking["progress"][1]);
    }

    public function testExtractRankingPointsRank()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Points") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(0, $ranking["rank"][0]);
    }

    public function testExtractRankingPointsTotalScore()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Points") {
                $theEvent = $event;
                break;
            }
        }
        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(30, $ranking["totalScore"][0]);
        $this->assertSame(6, $ranking["totalScore"][1]);
    }

    public function testExtractRankingPointsProgress()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Points") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(2, $ranking["progress"][0]);
        $this->assertSame(1, $ranking["progress"][1]);
    }

    public function testExtractRankingCumulatedTimeRank()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Cumulated_Time") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(0, $ranking["rank"][0]);
    }

    public function testExtractRankingCumulatedTimeTotalTime()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Cumulated_Time") {
                $theEvent = $event;
                break;
            }
        }
        $theEvent->update($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(4136000, $ranking["totalTime"][0]);
        $this->assertSame(2552000, $ranking["totalTime"][1]);
    }

    public function testExtractRankingCumulatedTimeProgress()
    {
        $theEvent = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getName() === "Cumulated_Time") {
                $theEvent = $event;
                break;
            }
        }
        $ranking = $theEvent->extractRanking($this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame(2, $ranking["progress"][0]);
        $this->assertSame(1, $ranking["progress"][1]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
