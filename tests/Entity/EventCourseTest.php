<?php


namespace App\Tests\Entity;


use App\Entity\EventCourse;
use App\Entity\ParticipantMakeEventCourse;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EventCourseTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testConvertTimeToMaxTime()
    {
        $eventCourse = new EventCourse();
        $eventCourse->setTime(1000);
        $eventCourse->convertTimeToMaxTime();
        $this->assertEquals(1000000, $eventCourse->getMaxTime());
    }

    public function testRemove()
    {
        $eventCourse = $this->entityManager->getRepository(EventCourse::class)->findAll()[0];
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $eventCourse->remove($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 2);
    }

    public function testGenerateParticipantMakeEventCourse()
    {
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $eventCourse = $this->entityManager->getRepository(EventCourse::class)->findAll()[0];
        $event = $eventCourse->getEvent();
        $participant1 = $eventCourse->getEvent()->getParticipants()[0];
        $participant2 = $eventCourse->getEvent()->getParticipants()[1];
        $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant1))->remove($this->entityManager);
        $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant2))->remove($this->entityManager);
        $eventCourse->generateParticipantMakeEventCourse($this->entityManager);
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}