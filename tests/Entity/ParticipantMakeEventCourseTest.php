<?php


namespace App\Tests\Entity;

use App\Entity\Event;
use App\Entity\ParticipantMakeEventCourse;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ParticipantMakeEventCourseTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRemove()
    {
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());


        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()[0];

        $participantMakeEventCourse->remove($this->entityManager);
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 1);
    }

    public function testExtractFromTrackNbOverTimePenalty()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 0) {
                $championship = $event;
            }
        }
        $eric = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $eric = $participant;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $eric));
        $participantMakeEventCourse->extractFromTrack();

        $this->assertSame(3, $participantMakeEventCourse->getNbOverTimePenalty());
    }

    public function testExtractFromTrackNbMissingPunchPenalty()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 0) {
                $championship = $event;
            }
        }
        $eric = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $eric = $participant;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $eric));
        $participantMakeEventCourse->extractFromTrack();

        $this->assertSame(1, $participantMakeEventCourse->getNbMissingPunchPenalty());
    }

    public function testExtractFromTrackScore()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 1) {
                $championship = $event;
            }
        }
        $eric = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $eric = $participant;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $eric));
        $participantMakeEventCourse->extractFromTrack();

        $this->assertSame(6, $participantMakeEventCourse->getScore());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}