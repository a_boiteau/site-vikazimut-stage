<?php


namespace App\Tests\Entity;

use App\Entity\Event;
use App\Entity\Participant;
use App\Entity\ParticipantMakeEventCourse;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ParticipantTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRemove()
    {
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $nbParticipant = count($this->entityManager->getRepository(Participant::class)->findAll());

        $participant = $this->entityManager->getRepository(Participant::class)->findAll()[0];

        $participant->remove($this->entityManager, $this->entityManager->getRepository(ParticipantMakeEventCourse::class));

        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 2);
        $this->assertSame($nbParticipant, count($this->entityManager->getRepository(Participant::class)->findAll()) + 1);
    }

    public function testCheckNicknameFree()
    {
        $participant = new Participant();
        $participant->setNickname("Regis");
        $this->assertTrue($participant->checkNickname($this->entityManager->getRepository(Event::class)->findAll()[0]));
    }

    public function testCheckNicknameNotFree()
    {
        $participant = new Participant();
        $participant->setNickname("Suliac");
        $this->assertFalse($participant->checkNickname($this->entityManager->getRepository(Event::class)->findAll()[0]));
    }

    public function testCreate()
    {
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $nbParticipant = count($this->entityManager->getRepository(Participant::class)->findAll());

        $participant = new Participant();
        $participant->setNickname("Regis");
        $participant->create($this->entityManager->getRepository(Event::class)->findAll()[0], $this->entityManager);

        $this->assertSame($nbParticipantMakeEventCourse + 2, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()));
        $this->assertSame($nbParticipant + 1, count($this->entityManager->getRepository(Participant::class)->findAll()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}