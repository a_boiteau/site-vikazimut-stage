<?php
//
//namespace App\Tests;
//
//use PHPUnit\Framework\TestCase;
//use App\Entity\User;
//
//class UserTest extends TestCase
//{
//
//    public function testInvalidUsername(){
//        $user = new User();
//
//        $user->setPassword("1\$OruA0TuUripYctziQ5f6uw\$GgmJW3gY7/yGnzU5gTSqtWghYVI7UK/Ai8QcKxwGxkc");
//        $user->setEmail("t@t.t");
//        $user->setPhone("+111 111 111 11");
//
//        $this->assertFalse($user->isValid());
//
//        $user->setUsername("");
//
//        $this->assertFalse($user->isValid());
//
//        $this->assertEquals("Nom d'utilisateur non conforme",$user->getError());
//    }
//
//    public function testInvalidPassword(){
//        $user = new User();
//
//        $user->setUsername("test");
//        $user->setEmail("t@t.t");
//        $user->setPhone("+111 111 111 11");
//
//        $this->assertFalse($user->isValid());
//
//        $user->setPassword("");
//
//        $this->assertFalse($user->isValid());
//
//        /*
//        maybe check if password is encrypted
//
//        $user->setPassword("test");
//
//        $this->assertFalse($user->isValid());
//        */
//        $this->assertEquals("Mot de passe non conforme",$user->getError());
//    }
//
//    public function testInvalidEmail(){
//        $user = new User();
//
//        $user->setUsername("test");
//        $user->setPassword("1\$OruA0TuUripYctziQ5f6uw\$GgmJW3gY7/yGnzU5gTSqtWghYVI7UK/Ai8QcKxwGxkc");
//        $user->setPhone("+111 111 111 11");
//
//        $this->assertFalse($user->isValid());
//
//        $user->setEmail("");
//
//        $this->assertFalse($user->isValid());
//
//        $user->setEmail("noEmail");
//
//        $this->assertFalse($user->isValid());
//
//        $this->assertEquals("Adresse mail non conforme",$user->getError());
//    }
//
//    public function testInvalidPhone(){
//        $user = new User();
//
//        $user->setUsername("test");
//        $user->setPassword("1\$OruA0TuUripYctziQ5f6uw\$GgmJW3gY7/yGnzU5gTSqtWghYVI7UK/Ai8QcKxwGxkc");
//        $user->setEmail("t@t.t");
//        $user->setPhone("test");
//
//        $this->assertFalse($user->isValid());
//
//        $this->assertEquals("Numéro de téléphone non conforme",$user->getError());
//    }
//
//    public function testValidUser(){
//        $user = new User();
//
//        $user->setUsername("test");
//        $user->setPassword("1\$OruA0TuUripYctziQ5f6uw\$GgmJW3gY7/yGnzU5gTSqtWghYVI7UK/Ai8QcKxwGxkc");
//        $user->setEmail("t@t.t");
//        $user->setPhone("+111 111 111 11");
//
//        $this->assertTrue($user->isValid());
//
//        $this->assertEquals("",$user->getError());
//    }
//
//}
