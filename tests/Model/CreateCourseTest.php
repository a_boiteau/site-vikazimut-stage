<?php

namespace App\Tests\Model;

use App\Model\CreateCourse;
use PHPUnit\Framework\TestCase;

class CreateCourseTest extends TestCase
{
    protected $xmlstr = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2021-06-03T15:21:23.897+02:00" creator="OCAD 12.4.0.1684">
  <Event>
    <Name>Event</Name>
  </Event>
  <RaceCourseData>
    <Map>
      <Scale>5000</Scale>
      <!-- extent of ocad map-->
      <MapPositionTopLeft x="-680.7" y="661.0" unit="mm"/>
      <MapPositionBottomRight x="188.0" y="16.7" unit="mm"/>
    </Map>
    <Control>
      <Id>S1</Id>
      <Position lng="-0.707867" lat="49.276441"/>
      <MapPosition x="-153.7" y="173.7" unit="mm"/>
    </Control>
    <Control>
      <Id>S2</Id>
      <Position lng="-0.711518" lat="49.273183"/>
      <MapPosition x="-210.2" y="103.8" unit="mm"/>
    </Control>
    <Control>
      <Id>S3</Id>
      <Position lng="-0.701366" lat="49.273314"/>
      <MapPosition x="-62.4" y="99.8" unit="mm"/>
    </Control>
    <Control>
      <Id>S4</Id>
      <Position lng="-0.701802" lat="49.271846"/>
      <MapPosition x="-70.3" y="67.5" unit="mm"/>
    </Control>
    <Control>
      <Id>31</Id>
      <Position lng="-0.712823" lat="49.273708"/>
      <MapPosition x="-228.6" y="116.4" unit="mm"/>
    </Control>
    <Control>
      <Id>32</Id>
      <Position lng="-0.714457" lat="49.274719"/>
      <MapPosition x="-251.3" y="140.0" unit="mm"/>
    </Control>
    <Control>
      <Id>33</Id>
      <Position lng="-0.707526" lat="49.276296"/>
      <MapPosition x="-148.9" y="170.3" unit="mm"/>
    </Control>
    <Control>
      <Id>34</Id>
      <Position lng="-0.708074" lat="49.279350"/>
      <MapPosition x="-153.6" y="238.5" unit="mm"/>
    </Control>
    <Control>
      <Id>35</Id>
      <Position lng="-0.703282" lat="49.278449"/>
      <MapPosition x="-84.9" y="215.2" unit="mm"/>
    </Control>
    <Control>
      <Id>36</Id>
      <Position lng="-0.702574" lat="49.277253"/>
      <MapPosition x="-75.9" y="188.2" unit="mm"/>
    </Control>
    <Control>
      <Id>37</Id>
      <Position lng="-0.702583" lat="49.276368"/>
      <MapPosition x="-76.9" y="168.5" unit="mm"/>
    </Control>
    <Control>
      <Id>38</Id>
      <Position lng="-0.702323" lat="49.275751"/>
      <MapPosition x="-73.8" y="154.6" unit="mm"/>
    </Control>
    <Control>
      <Id>39</Id>
      <Position lng="-0.700573" lat="49.274437"/>
      <MapPosition x="-49.7" y="124.2" unit="mm"/>
    </Control>
    <Control>
      <Id>40</Id>
      <Position lng="-0.700951" lat="49.277575"/>
      <MapPosition x="-51.9" y="194.2" unit="mm"/>
    </Control>
    <Control>
      <Id>41</Id>
      <Position lng="-0.700876" lat="49.276821"/>
      <MapPosition x="-51.6" y="177.4" unit="mm"/>
    </Control>
    <Control>
      <Id>42</Id>
      <Position lng="-0.705917" lat="49.275900"/>
      <MapPosition x="-125.9" y="160.4" unit="mm"/>
    </Control>
    <Control>
      <Id>43</Id>
      <Position lng="-0.704302" lat="49.275873"/>
      <MapPosition x="-102.4" y="158.7" unit="mm"/>
    </Control>
    <Control>
      <Id>44</Id>
      <Position lng="-0.704915" lat="49.274388"/>
      <MapPosition x="-112.9" y="126.1" unit="mm"/>
    </Control>
    <Control>
      <Id>45</Id>
      <Position lng="-0.703465" lat="49.274969"/>
      <MapPosition x="-91.2" y="138.0" unit="mm"/>
    </Control>
    <Control>
      <Id>46</Id>
      <Position lng="-0.702732" lat="49.275712"/>
      <MapPosition x="-79.8" y="154.1" unit="mm"/>
    </Control>
    <Control>
      <Id>47</Id>
      <Position lng="-0.703755" lat="49.275963"/>
      <MapPosition x="-94.4" y="160.3" unit="mm"/>
    </Control>
    <Control>
      <Id>48</Id>
      <Position lng="-0.704020" lat="49.277662"/>
      <MapPosition x="-96.5" y="198.3" unit="mm"/>
    </Control>
    <Control>
      <Id>49</Id>
      <Position lng="-0.702930" lat="49.277326"/>
      <MapPosition x="-81.0" y="190.0" unit="mm"/>
    </Control>
    <Control>
      <Id>50</Id>
      <Position lng="-0.702090" lat="49.271703"/>
      <MapPosition x="-74.6" y="64.5" unit="mm"/>
    </Control>
    <Control>
      <Id>51</Id>
      <Position lng="-0.705858" lat="49.274464"/>
      <MapPosition x="-126.5" y="128.4" unit="mm"/>
    </Control>
    <Control>
      <Id>52</Id>
      <Position lng="-0.709146" lat="49.276600"/>
      <MapPosition x="-172.1" y="178.2" unit="mm"/>
    </Control>
    <Control>
      <Id>53</Id>
      <Position lng="-0.706126" lat="49.277067"/>
      <MapPosition x="-127.7" y="186.5" unit="mm"/>
    </Control>
    <Control>
      <Id>54</Id>
      <Position lng="-0.706007" lat="49.278322"/>
      <MapPosition x="-124.7" y="214.3" unit="mm"/>
    </Control>
    <Control>
      <Id>55</Id>
      <Position lng="-0.703654" lat="49.277517"/>
      <MapPosition x="-91.3" y="194.8" unit="mm"/>
    </Control>
    <Control>
      <Id>56</Id>
      <Position lng="-0.699652" lat="49.277198"/>
      <MapPosition x="-33.4" y="185.0" unit="mm"/>
    </Control>
    <Control>
      <Id>57</Id>
      <Position lng="-0.698833" lat="49.276619"/>
      <MapPosition x="-22.1" y="171.6" unit="mm"/>
    </Control>
    <Control>
      <Id>58</Id>
      <Position lng="-0.712502" lat="49.273418"/>
      <MapPosition x="-224.2" y="109.7" unit="mm"/>
    </Control>
    <Control>
      <Id>59</Id>
      <Position lng="-0.702419" lat="49.271579"/>
      <MapPosition x="-79.5" y="62.0" unit="mm"/>
    </Control>
    <Control>
      <Id>60</Id>
      <Position lng="-0.700890" lat="49.275767"/>
      <MapPosition x="-52.9" y="154.0" unit="mm"/>
    </Control>
    <Control>
      <Id>61</Id>
      <Position lng="-0.701196" lat="49.273907"/>
      <MapPosition x="-59.3" y="112.9" unit="mm"/>
    </Control>
    <Control>
      <Id>62</Id>
      <Position lng="-0.697010" lat="49.280438"/>
      <MapPosition x="8.4" y="255.2" unit="mm"/>
    </Control>
    <Control>
      <Id>63</Id>
      <Position lng="-0.715630" lat="49.289785"/>
      <MapPosition x="-252.6" y="475.6" unit="mm"/>
    </Control>
    <Control>
      <Id>64</Id>
      <Position lng="-0.718068" lat="49.288626"/>
      <MapPosition x="-289.2" y="451.5" unit="mm"/>
    </Control>
    <Control>
      <Id>65</Id>
      <Position lng="-0.696878" lat="49.279422"/>
      <MapPosition x="9.2" y="232.5" unit="mm"/>
    </Control>
    <Control>
      <Id>66</Id>
      <Position lng="-0.695894" lat="49.290683"/>
      <MapPosition x="35.3" y="482.1" unit="mm"/>
    </Control>
    <Control>
      <Id>67</Id>
      <Position lng="-0.717232" lat="49.286640"/>
      <MapPosition x="-279.1" y="406.8" unit="mm"/>
    </Control>
    <Control>
      <Id>69</Id>
      <Position lng="-0.700049" lat="49.275079"/>
      <MapPosition x="-41.4" y="138.2" unit="mm"/>
    </Control>
    <Control>
      <Id>70</Id>
      <Position lng="-0.699610" lat="49.277610"/>
      <MapPosition x="-32.4" y="194.1" unit="mm"/>
    </Control>
    <Control>
      <Id>71</Id>
      <Position lng="-0.706602" lat="49.293013"/>
      <MapPosition x="-118.0" y="541.2" unit="mm"/>
    </Control>
    <Control>
      <Id>72</Id>
      <Position lng="-0.710734" lat="49.290875"/>
      <MapPosition x="-180.3" y="496.5" unit="mm"/>
    </Control>
    <Control>
      <Id>73</Id>
      <Position lng="-0.697489" lat="49.284069"/>
      <MapPosition x="5.2" y="336.2" unit="mm"/>
    </Control>
    <Control>
      <Id>74</Id>
      <Position lng="-0.702148" lat="49.271389"/>
      <MapPosition x="-75.8" y="57.6" unit="mm"/>
    </Control>
    <Control>
      <Id>75</Id>
      <Position lng="-0.698771" lat="49.287355"/>
      <MapPosition x="-10.0" y="410.1" unit="mm"/>
    </Control>
    <Control>
      <Id>76</Id>
      <Position lng="-0.715931" lat="49.282451"/>
      <MapPosition x="-264.6" y="312.8" unit="mm"/>
    </Control>
    <Control>
      <Id>77</Id>
      <Position lng="-0.717352" lat="49.280936"/>
      <MapPosition x="-286.9" y="280.1" unit="mm"/>
    </Control>
    <Control>
      <Id>78</Id>
      <Position lng="-0.717790" lat="49.279814"/>
      <MapPosition x="-294.4" y="255.5" unit="mm"/>
    </Control>
    <Control>
      <Id>79</Id>
      <Position lng="-0.711089" lat="49.275357"/>
      <MapPosition x="-201.7" y="151.9" unit="mm"/>
    </Control>
    <Control>
      <Id>80</Id>
      <Position lng="-0.703078" lat="49.273996"/>
      <MapPosition x="-86.6" y="116.1" unit="mm"/>
    </Control>
    <Control>
      <Id>81</Id>
      <Position lng="-0.707254" lat="49.283674"/>
      <MapPosition x="-137.2" y="334.1" unit="mm"/>
    </Control>
    <Control>
      <Id>82</Id>
      <Position lng="-0.706971" lat="49.289138"/>
      <MapPosition x="-127.4" y="455.3" unit="mm"/>
    </Control>
    <Control>
      <Id>83</Id>
      <Position lng="-0.700613" lat="49.288541"/>
      <MapPosition x="-35.6" y="437.7" unit="mm"/>
    </Control>
    <Control>
      <Id>84</Id>
      <Position lng="-0.698729" lat="49.287522"/>
      <MapPosition x="-9.3" y="413.8" unit="mm"/>
    </Control>
    <Control>
      <Id>85</Id>
      <Position lng="-0.697009" lat="49.284465"/>
      <MapPosition x="12.6" y="344.7" unit="mm"/>
    </Control>
    <Control>
      <Id>86</Id>
      <Position lng="-0.697485" lat="49.280282"/>
      <MapPosition x="1.3" y="252.0" unit="mm"/>
    </Control>
    <Control>
      <Id>87</Id>
      <Position lng="-0.698209" lat="49.278278"/>
      <MapPosition x="-11.3" y="208.0" unit="mm"/>
    </Control>
    <Control>
      <Id>F1</Id>
      <Position lng="-0.700541" lat="49.274418"/>
      <MapPosition x="-49.3" y="123.8" unit="mm"/>
    </Control>
    <Control>
      <Id>F2</Id>
      <Position lng="-0.700640" lat="49.277023"/>
      <MapPosition x="-48.0" y="181.8" unit="mm"/>
    </Control>
    <Control>
      <Id>F3</Id>
      <Position lng="-0.701155" lat="49.274082"/>
      <MapPosition x="-58.5" y="116.7" unit="mm"/>
    </Control>
    <Control>
      <Id>F4</Id>
      <Position lng="-0.708514" lat="49.275986"/>
      <MapPosition x="-163.5" y="164.1" unit="mm"/>
    </Control>
    <Course>
      <Name>Baj'Orienteur</Name>
      <Length>3070</Length>
      <Climb>0</Climb>
      <CourseControl type="Start">
        <Control>S3</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>69</Control>
        <LegLength>219</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>56</Control>
        <LegLength>237</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>57</Control>
        <LegLength>88</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>60</Control>
        <LegLength>177</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>41</Control>
        <LegLength>117</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>70</Control>
        <LegLength>127</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>87</Control>
        <LegLength>126</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>62</Control>
        <LegLength>256</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>35</Control>
        <LegLength>507</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>54</Control>
        <LegLength>199</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>46</Control>
        <LegLength>376</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>38</Control>
        <LegLength>30</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>44</Control>
        <LegLength>242</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>33</Control>
        <LegLength>285</LegLength>
      </CourseControl>
      <CourseControl type="Finish">
        <Control>F4</Control>
        <LegLength>80</LegLength>
      </CourseControl>
    </Course>
  </RaceCourseData>
</CourseData>
XML;

    public function testGetFirstControlPointName()
    {
        $xmlContents = simplexml_load_string($this->xmlstr);
        $a = CreateCourse::getFirstControlPointName($xmlContents);
        $this->assertSame($a, "S3");
    }


    public function testGetControlPointWithName()
    {
        $xmlContents = simplexml_load_string($this->xmlstr);
        $a = CreateCourse::getControlPointFromName($xmlContents, "S3" );
        print($a->Id);
        $this->assertSame((string)$a->Id, "S3");
    }
}
