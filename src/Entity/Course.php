<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Model\CourseValidator;
use Doctrine\Persistence\ObjectManager;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $creator;

    /**
     * @ORM\Column(type="text", length=65535)
     */
    private ?string $xml;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * valid type for the image is define in isValid
     */
    private ?string $image;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private ?string $kml;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $updateAt;

    private string $error;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Track", mappedBy="course", orphanRemoval=true)
     */
    private Collection $orienteer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MissingControlPoint", mappedBy="course", orphanRemoval=true)
     */
    private Collection $missingControlPoints;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $latitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $longitude;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $length;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $printable = false;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $club;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventCourse", mappedBy="course")
     */
    private $eventCourses;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $startDate = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $endDate = null;

    public function __construct()
    {
        $this->orienteer = new ArrayCollection();
        $this->missingControlPoints = new ArrayCollection();
    }

    public function isValid(): bool
    {
        $this->error = CourseValidator::checkValid(
            $this->xml,
            $this->image,
            $this->kml,
            $this->printable
        );

        return $this->error == CourseValidator::$NO_ERROR;
    }

    public function remove(ObjectManager $entityManager)
    {
        if (file_exists($this->image)) {
            unlink($this->image);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getXml(): ?string
    {
        return $this->xml;
    }

    public function setXml(string $xml): self
    {
        $this->xml = $xml;;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getKml(): ?string
    {
        return $this->kml;
    }

    public function setKml(string $kml): self
    {
        $this->kml = $kml;

        return $this;
    }

    public function getUpdateAt(): ?DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClub(): ?string
    {
        return $this->club;
    }

    public function setClub(?string $club): self
    {
        $this->club = $club;

        return $this;
    }

    /**
     * @return Collection|Track[]
     */
    public function getOrienteer(): Collection
    {
        return $this->orienteer;
    }

    public function addOrienteer(Track $orienteer): self
    {
        if (!$this->orienteer->contains($orienteer)) {
            $this->orienteer[] = $orienteer;
            $orienteer->setCourse($this);
        }

        return $this;
    }

    public function removeOrienteer(Track $orienteer): self
    {
        if ($this->orienteer->contains($orienteer)) {
            $this->orienteer->removeElement($orienteer);
            // set the owning side to null (unless already changed)
            if ($orienteer->getCourse() === $this) {
                $orienteer->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MissingControlPoint[]
     */
    public function getMissingControlPoints(): Collection
    {
        return $this->missingControlPoints;
    }

    public function addMissingControlPoint(MissingControlPoint $missingControlPoint): self
    {
        if (!$this->missingControlPoints->contains($missingControlPoint)) {
            $this->missingControlPoints[] = $missingControlPoint;
            $missingControlPoint->setCourse($this);
        }

        return $this;
    }

    public function removeMissingControlPoint(MissingControlPoint $missingControlPoint): self
    {
        if ($this->missingControlPoints->contains($missingControlPoint)) {
            $this->missingControlPoints->removeElement($missingControlPoint);
            // set the owning side to null (unless already changed)
            if ($missingControlPoint->getCourse() === $this) {
                $missingControlPoint->setCourse(null);
            }
        }

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getPrintable(): ?bool
    {
        return $this->printable;
    }

    public function setPrintable(bool $printable): self
    {
        $this->printable = $printable;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }
}
