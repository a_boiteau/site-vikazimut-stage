<?php

namespace App\Entity;

use App\Repository\EventCourseRepository;
use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;

/**
 * @ORM\Entity(repositoryClass=EventCourseRepository::class)
 */
class EventCourse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="eventCourses")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Event $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="eventCourses")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Course $course;

    /**
     * @ORM\Column(type="integer")
     */
    private int $format;

    /**
     * @ORM\Column(type="integer")
     */
    private int $missingPunchPenalty;

    /**
     * @ORM\Column(type="integer")
     */
    private int $overTimePenalty;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $maxTime;

    private string $time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getMissingPunchPenalty(): ?int
    {
        return $this->missingPunchPenalty;
    }

    public function setMissingPunchPenalty(int $missingPunchPenalty): self
    {
        $this->missingPunchPenalty = $missingPunchPenalty;

        return $this;
    }

    public function getOverTimePenalty(): ?int
    {
        return $this->overTimePenalty;
    }

    public function setOverTimePenalty(int $overTimePenalty): self
    {
        $this->overTimePenalty = $overTimePenalty;

        return $this;
    }

    public function getMaxTime(): ?int
    {
        return $this->maxTime;
    }

    public function setMaxTime(int $maxTime): self
    {
        $this->maxTime = $maxTime;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function convertTimeToMaxTime()
    {
        if ($this->time > 0) {
            $this->maxTime = $this->time * 1000;
        } else {
            $this->maxTime = 0;
        }
    }

    public function remove(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourseRepository)
    {
        foreach ($this->event->getParticipants() as $participant) {
            $participantMakeEventCourseRepository->find(array("eventCourse" => $this, "participant" => $participant))->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function generateParticipantMakeEventCourse($entityManager)
    {
        $participants = $this->event->getParticipants();
        foreach ($participants as $participant) {
            $participantMakeEventCourse = new ParticipantMakeEventCourse();
            $participantMakeEventCourse->setParticipant($participant);
            $participantMakeEventCourse->setEventCourse($this);
            $entityManager->persist($participantMakeEventCourse);
        }
        $entityManager->flush();
    }
}