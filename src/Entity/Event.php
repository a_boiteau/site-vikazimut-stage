<?php

namespace App\Entity;

use App\Repository\EventRepository;
use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $creator;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventCourse", mappedBy="event", orphanRemoval=true)
     */
    private Collection $eventCourses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Participant", mappedBy="event", orphanRemoval=true)
     */
    private Collection $participant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|EventCourse[]
     */
    public function getEventCourses(): Collection
    {
        return $this->eventCourses;
    }

    public function remove(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourse)
    {
        foreach ($this->getEventCourses() as $eventCourse) {
            foreach ($this->getParticipants() as $participant)
                $participantMakeEventCourse->find(array("eventCourse" => $eventCourse, "participant" => $participant))->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    /**
     * @return Collection|Participant[]
     */
    public function getParticipants(): Collection
    {
        return $this->participant;
    }

    public function update(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourseRepository)
    {
        foreach ($this->eventCourses as $eventCourse) {
            if ($this->type === 0) {
                $minTime = INF;
                foreach ($this->participant as $participant) {
                    $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $participantMakeEventCourse->extractFromTrack();
                        $time = $participantMakeEventCourse->getTrack()->getTotalTime() + ($participantMakeEventCourse->getNbMissingPunchPenalty() * $participantMakeEventCourse->getEventCourse()->getMissingPunchPenalty() * 1000);
                        if ($participantMakeEventCourse->getEventCourse()->getOverTimePenalty() === 0) {
                            $time += ($participantMakeEventCourse->getNbOverTimePenalty() * $participantMakeEventCourse->getEventCourse()->getOverTimePenalty() * 1000);
                        }
                        if ($minTime > $time) {
                            $minTime = $time;
                        }
                    }
                }
                foreach ($this->participant as $participant) {
                    $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $time = $participantMakeEventCourse->getTrack()->getTotalTime() + ($participantMakeEventCourse->getNbMissingPunchPenalty() * $participantMakeEventCourse->getEventCourse()->getMissingPunchPenalty() * 1000);
                        if ($participantMakeEventCourse->getEventCourse()->getOverTimePenalty() === 0) {
                            $time += ($participantMakeEventCourse->getNbOverTimePenalty() * $participantMakeEventCourse->getEventCourse()->getOverTimePenalty() * 1000);
                        }
                        $participantMakeEventCourse->setScore(1000 * (($minTime * 1000) / ($time * 1000)));
                    }
                }
            } else {
                foreach ($this->participant as $participant) {
                    $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $participantMakeEventCourse->extractFromTrack();
                    }
                }
            }
        }
        $entityManager->flush();
    }

    public function getCSV($participantMakeEventCourseRepository): string
    {
        $csv = "";
        $separateChar = ",";
        if ($this->type === 0 or $this->type === 1) {
            $csv = "nickname" . $separateChar . "totalScore";
        } elseif ($this->type === 2) {
            $csv = "nickname" . $separateChar . "totalTime";
        }
        foreach ($this->eventCourses as $eventCourse) {
            if ($this->type === 0 or $this->type === 1) {
                $csv = $csv . $separateChar . "Score" . $separateChar . "Time" . $separateChar . "(PM)" . $separateChar . "(OT)";
            } elseif ($this->type === 2) {
                $csv = $csv . $separateChar . "Time" . $separateChar . "(PM)" . $separateChar . "(OT)";
            }
            foreach (simplexml_load_string($eventCourse->getCourse()->getXml())->RaceCourseData->Course->CourseControl as $controlPoint) {
                $csv = $csv . $separateChar . (string)$controlPoint->Control;
            }
        }
        foreach ($this->participant as $participant) {
            $csv = $csv . "\n" . $participant->getNickname();
            $tmp = "";
            $score = 0;
            $time = 0;
            foreach ($this->eventCourses as $eventCourse) {
                $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                if ($this->type === 0 or $this->type === 1) {
                    $score += $participantMakeEventCourse->getScore();
                }
                if ($this->type === 0 or $this->type === 1) {
                    $tmp = $tmp . $separateChar . (string)$participantMakeEventCourse->getScore();
                }
                if ($participantMakeEventCourse->getTrack() !== null) {
                    if ($this->type === 2) {
                        $time += $participantMakeEventCourse->getTrack()->getTotalTime();
                    }
                    $tmp = $tmp . $separateChar . (string)$participantMakeEventCourse->getTrack()->getTotalTime() . $separateChar . (string)$participantMakeEventCourse->getNbMissingPunchPenalty() . $separateChar . (string)$participantMakeEventCourse->getNbOverTimePenalty();;
                    foreach ($participantMakeEventCourse->getTrack()->getControlPoints() as $point) {
                        $tmp = $tmp . $separateChar . (string)$point->punchTime;
                    }
                } else {
                    $tmp = $tmp . $separateChar . "0" . $separateChar . (string)$participantMakeEventCourse->getNbMissingPunchPenalty() . $separateChar . (string)$participantMakeEventCourse->getNbOverTimePenalty();;
                    foreach (simplexml_load_string($eventCourse->getCourse()->getXml())->RaceCourseData->Course->CourseControl as $controlPoint) {
                        $tmp = $tmp . $separateChar . "0";
                    }
                }
            }
            if ($this->type === 0 or $this->type === 1) {
                $csv = $csv . $separateChar . (string)$score . $tmp;
            } elseif ($this->type === 2) {
                $csv = $csv . $separateChar . (string)$time . $tmp;
            }
        }
        return $csv;
    }

    public function extractRanking($participantMakeEventCourseRepository): array
    {
        $participants = $this->participant;
        $eventCourses = $this->eventCourses;
        $score = [];
        $totalTime = [];
        $ranking = [];
        $ranking["rank"] = array();
        if ($participants[0] !== null and $eventCourses[0] !== null) {
            for ($i = 0; $i < sizeof($participants); $i++) {
                $score[$i] = 0;
                $totalTime[$i] = 0;
                $ranking["progress"][$i] = 0;
                $ranking["eventCourse"][$i] = array();
                $ranking["modified"][$i] = array();
                foreach ($eventCourses as $eventCourse) {
                    $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourse, "participant" => $participants->get($i)));
                    if ($participantMakeEventCourse->getTrack() !== null) {
                        $eventCourseScore = $participantMakeEventCourse->getScore();
                        $eventCourseTime = $participantMakeEventCourse->getTrack()->getTotalTime();
                        if ($this->type === 0 or $this->type === 2) {
                            $eventCourseTime += ($participantMakeEventCourse->getNbMissingPunchPenalty() * $participantMakeEventCourse->getEventCourse()->getMissingPunchPenalty() * 1000);
                            if ($participantMakeEventCourse->getEventCourse()->getOverTimePenalty() !== 0) {
                                $eventCourseTime += ($participantMakeEventCourse->getNbOverTimePenalty() * $participantMakeEventCourse->getEventCourse()->getOverTimePenalty() * 1000);
                            }
                        }
                        $ranking["progress"][$i] += 1;
                        $score[$i] += $eventCourseScore;
                        $totalTime[$i] += $eventCourseTime;
                        array_push($ranking["eventCourse"][$i], array($eventCourseScore, $participantMakeEventCourse->getNbMissingPunchPenalty(), $participantMakeEventCourse->getNbOverTimePenalty(), $eventCourseTime));
                    } else {
                        array_push($ranking["eventCourse"][$i], array("-", "-", "-", "-"));
                    }
                    array_push($ranking["modified"][$i], $participantMakeEventCourse->isModified());
                }
                $ranking["totalScore"][$i] = $score[$i];
                $ranking["totalTime"][$i] = $totalTime[$i];
            }
            if ($this->type === 0 or $this->type === 1) {
                while (sizeof($score) !== 0) {
                    $max = max($score);
                    array_push($ranking["rank"], array_search($max, $score));
                    unset($score[array_search($max, $score)]);
                }
            } elseif ($this->type === 2) {
                $nbCourses = sizeof($this->getEventCourses());
                for ($progress = $nbCourses; $progress > 0; $progress--) {
                    $minTime = INF;
                    $min = null;
                    foreach ($totalTime as $totalTime1) {
                        if ($totalTime1 < $minTime) {
                            $id = array_search($totalTime1, $totalTime);
                            if ($ranking["progress"][$id] === $progress) {
                                array_push($ranking["rank"], $id);
                                unset($totalTime[$id]);
                            }
                        }
                    }
                }
            }
        }
        return $ranking;
    }
}
