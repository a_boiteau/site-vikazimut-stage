<?php

namespace App\Entity;

use App\Repository\ParticipantMakeEventCourseRepository;
use App\Repository\ParticipantRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;

/**
 * @ORM\Entity(repositoryClass=ParticipantRepository::class)
 */
class Participant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $nickname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="participant")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Event $event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function remove(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourse)
    {
        foreach ($this->event->getEventCourses() as $eventCourse) {
            $participantMakeEventCourse->find(array("eventCourse" => $eventCourse, "participant" => $this))->remove($entityManager);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function checkNickname(Event $event): bool
    {
        foreach ($event->getParticipants() as $participant) {
            if ($this->nickname === $participant->getNickname()) {
                return false;
            }
        }
        return true;
    }

    public function create(Event $event, ObjectManager $entityManager): string
    {
        if ($this->checkNickname($event)) {
            $this->setEvent($event);
            $entityManager->persist($this);

            foreach ($event->getEventCourses() as $eventCourse) {
                $participantMakeEventCourse = new ParticipantMakeEventCourse();
                $participantMakeEventCourse->setParticipant($this);
                $participantMakeEventCourse->setEventCourse($eventCourse);
                $entityManager->persist($participantMakeEventCourse);
            }
            $entityManager->flush();
            return "";
        } else {
            return "nickname not free";
        }
    }
}
