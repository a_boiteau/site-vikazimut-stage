<?php

namespace App\Entity;

use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ObjectManager;

/**
 * @ORM\Entity(repositoryClass=ParticipantMakeEventCourseRepository::class)
 */
class ParticipantMakeEventCourse
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\EventCourse")
     */
    private ?EventCourse $eventCourse;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Participant")
     */
    private ?Participant $participant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Track", inversedBy="ParticipantMakeEventCourses")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Track $track = null;

    /**
     * @ORM\Column(type="integer")
     */
    private int $score = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $nbOverTimePenalty = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $nbMissingPunchPenalty = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $modified = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $pmPenaltyManuallySet = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $otPenaltyManuallySet = false;

    public function getEventCourse(): ?EventCourse
    {
        return $this->eventCourse;
    }

    public function setEventCourse(?EventCourse $eventCourse): self
    {
        $this->eventCourse = $eventCourse;

        return $this;
    }

    public function getParticipant(): ?Participant
    {
        return $this->participant;
    }

    public function setParticipant(?Participant $participant): self
    {
        $this->participant = $participant;

        return $this;
    }

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNbOverTimePenalty(): ?int
    {
        return $this->nbOverTimePenalty;
    }

    public function setNbOverTimePenalty(int $nbOverTimePenalty): self
    {
        $this->nbOverTimePenalty = $nbOverTimePenalty;

        return $this;
    }

    public function getNbMissingPunchPenalty(): ?int
    {
        return $this->nbMissingPunchPenalty;
    }

    public function setNbMissingPunchPenalty(int $nbMissingPunchPenalty): self
    {
        $this->nbMissingPunchPenalty = $nbMissingPunchPenalty;

        return $this;
    }

    /**
     * @return bool
     */
    public function isModified(): bool
    {
        return $this->modified;
    }

    /**
     * @param bool $modified
     */
    public function setModified(bool $modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @return bool
     */
    public function isPmPenaltyManuallySet(): bool
    {
        return $this->pmPenaltyManuallySet;
    }

    /**
     * @param bool $pmPenaltyManuallySet
     */
    public function setPmPenaltyManuallySet(bool $pmPenaltyManuallySet): void
    {
        $this->pmPenaltyManuallySet = $pmPenaltyManuallySet;
    }

    /**
     * @return bool
     */
    public function isOtPenaltyManuallySet(): bool
    {
        return $this->otPenaltyManuallySet;
    }

    /**
     * @param bool $otPenaltyManuallySet
     */
    public function setOtPenaltyManuallySet(bool $otPenaltyManuallySet): void
    {
        $this->otPenaltyManuallySet = $otPenaltyManuallySet;
    }

    public function remove(ObjectManager $entityManager)
    {
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function extractFromTrack()
    {
        $this->score = 0;
        if ($this->track !== null) {
            if ($this->otPenaltyManuallySet === false) {
                $this->nbOverTimePenalty = 0;
                if ($this->eventCourse->getMaxTime() !== 0) {
                    $overTime = $this->track->getTotalTime() - $this->eventCourse->getMaxTime();
                    if ($this->eventCourse->getMaxTime() !== 0 && $overTime > 0) {
                        if ($overTime / 1000 >= 60) {
                            $this->nbOverTimePenalty = floor(($overTime / 1000) / 60);
                            $overTime = ($overTime / 1000) % $this->nbOverTimePenalty;
                        }
                        if ($overTime != 0) {
                            $this->nbOverTimePenalty += 1;
                        }
                    }
                }
            }

            if ($this->pmPenaltyManuallySet === false) {
                $this->nbMissingPunchPenalty = 0;
                foreach ($this->track->getControlPoints() as $controlPoint) {
                    if ($controlPoint->punchTime === 0 and $controlPoint->controlPoint !== 0) {
                        $this->nbMissingPunchPenalty += 1;
                    }
                }
            }

            if ($this->participant->getEvent()->getType() === 1) {
                $t = simplexml_load_string($this->eventCourse->getCourse()->getXml());
                $controlPointIndex = 0;
                foreach ($t->RaceCourseData->Course->CourseControl as $y) {
                    if ((string)$y->attributes()["type"] === "Control") {
                        if ((int)$this->track->getControlPoints()[$controlPointIndex]->punchTime !== 0) {
                            if (isset($y->Score)) {
                                $this->score += (int)$y->Score;
                            } else {

                                $this->score += 1;
                            }
                        }
                    }
                    $controlPointIndex++;
                }
                $this->score -= (($this->nbMissingPunchPenalty * $this->eventCourse->getMissingPunchPenalty()) + $this->nbOverTimePenalty * $this->eventCourse->getOverTimePenalty());
            }
        }
    }
}
