<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MissingControlPointRepository")
 */
class MissingControlPoint
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $controlPointId;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $courseId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="missingControlPoints")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Course $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getControlPointId(): ?string
    {
        return $this->controlPointId;
    }

    public function setControlPointId(string $controlPointId): self
    {
        $this->controlPointId = $controlPointId;

        return $this;
    }

    public function getCourseId(): ?int
    {
        return $this->courseId;
    }

    public function setCourseId(int $courseId): self
    {
        $this->courseId = $courseId;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }
}
