<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrackRepository")
 */
class Track
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $format;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $totalTime;

    /**
     * @ORM\Column(type="array")
     */
    private array $controlPoints = [];

    /**
     * @ORM\Column(type="text")
     */
    private ?string $trace;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="orienteer")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Course $course;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $imported = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ParticipantMakeEventCourse", mappedBy="track")
     */
    private $ParticipantMakeEventCourses;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFormat(): ?int
    {
        return $this->format;
    }

    public function setFormat(int $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getTotalTime(): ?int
    {
        return $this->totalTime;
    }

    public function getTotalTimeInHMS(): string
    {
        $sec = (int)($this->totalTime / 1000);
        $hrs = 0;
        $min = 0;

        if ($sec >= 3600) {
            $hrs = floor($sec / 3600);
            $sec = $sec % 3600;
        }
        if ($sec >= 60) {
            $min = floor($sec / 60);
            $sec = $sec % 60;
        }

        if (strlen((string)$hrs) === 1) {
            $hrs = "0".$hrs;
        }
        if (strlen((string)$min) === 1) {
            $min = "0".$min;
        }
        if (strlen((string)$sec) === 1) {
            $sec = "0".$sec;
        }

        return $hrs.":".$min.":".$sec;
    }

    public function setTotalTime(int $totalTime): self
    {
        $this->totalTime = $totalTime;

        return $this;
    }

    public function getControlPoints(): ?array
    {
        return $this->controlPoints;
    }

    public function setControlPoints(array $controlPoints): self
    {
        $this->controlPoints = $controlPoints;

        return $this;
    }

    public function getTrace(): ?string
    {
        return $this->trace;
    }

    public function setTrace(string $trace): self
    {
        $this->trace = $trace;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getImported(): ?bool
    {
        return $this->imported;
    }

    public function setImported(bool $imported): self
    {
        $this->imported = $imported;

        return $this;
    }

    public function nbPM(): int
    {
        $pm = 0;
        foreach ($this->controlPoints as $controlPoint) {
            if ($controlPoint->controlPoint !== 0) {
                if ($controlPoint->punchTime === 0) {
                    $pm+=1;
                }
            }
        }
        return $pm;
    }
}
