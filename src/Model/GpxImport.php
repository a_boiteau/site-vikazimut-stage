<?php

namespace App\Model;

use Doctrine\Persistence\ObjectManager;
use App\Entity\Track;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GpxImport
{
    private string $nickname;
    private int $idCourse;
    private UploadedFile $gpx;
    private int $format;
    private $loadedGpx;
    private string $error = "";
    private int $gpsErrorMargin = 20;

    public function checkGpx(): bool
    {
        $ext = pathinfo($this->gpx->getClientOriginalName(), PATHINFO_EXTENSION);
        if ($ext === "gpx") {
            $content = file_get_contents($this->gpx->getPathname());
            $posGpxTag = strpos($content, "<gpx");
            $posTrkTag = strpos($content, "<trk");
            $posTrksegTag = strpos($content, "<trkseg");
            $posTrkptTag = strpos($content, "<trkpt");
            $posTimeTag = strpos($content, "<time");

            if ($posGpxTag !== false && $posTrkTag !== false && $posTrksegTag !== false && $posTrkptTag !== false && $posTimeTag !== false) {
                return true;
            } else {
                $this->error = "invalid Gpx file";

                return false;
            }
        } else {
            $this->error = "invalid Gpx ext";

            return false;
        }
    }

    public function checkIfIsInBound($courseKml): bool
    {
        $latLonBox = simplexml_load_string($courseKml)->Folder->GroundOverlay->LatLonBox;
        $north = (float)$latLonBox->north;
        $south = (float)$latLonBox->south;
        $east = (float)$latLonBox->east;
        $west = (float)$latLonBox->west;

        $xmlTrack = $this->loadedGpx->trk->trkseg->trkpt;
        foreach ($xmlTrack as $place) {
            if (((float)$place["lat"]) > $north or ((float)$place["lat"]) < $south) {
                $this->error = "gpx track out of bound";

                return false;
            }
            if (((float)$place["lon"]) > $east or ((float)$place["lon"]) < $west) {
                $this->error = "gpx track out of bound";

                return false;
            }
        }

        return true;
    }

    public function addToDataBase(ObjectManager $entityManager, $courseRepository): bool
    {
        $track = $this->createTrack($courseRepository);
        try {
            $entityManager->persist($track);
            $entityManager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function createTrack($courseRepository): Track
    {
        $track = new Track();
        $track->setName($this->nickname);
        $track->setFormat($this->format);
        $gpxContent = str_replace(array("\n", "\r"), '', (string)$this->loadedGpx->asXML());
        $track->setTrace($gpxContent);
        $track->setTotalTime($this->extractTotalTime());
        $track->setControlPoints($this->extractControlPoints(simplexml_load_string($courseRepository->find($this->idCourse)->getXml())));
        $track->setCourse($courseRepository->find($this->idCourse));
        $track->setImported(true);

        return $track;
    }

    public function extractTotalTime()
    {
        $trkptArray = $this->loadedGpx->trk->trkseg->trkpt;
        $strtime = (string)$trkptArray[count($trkptArray) - 1]->time;

        return $this->timeToMs($strtime);
    }

    public function extractControlPoints($xmlRoute): array
    {
        $controlPoints = array();
        $point = 0;
        foreach ($xmlRoute->RaceCourseData->Course->CourseControl as $controlPoint) {
            $id = (string)$controlPoint->Control;
            foreach ($xmlRoute->RaceCourseData->Control as $controlPointXml) {

                if ($id === (string)$controlPointXml->Id) {
                    array_push(
                        $controlPoints,
                        array(
                            "coord" => array((float)$controlPointXml->Position["lat"], (float)$controlPointXml->Position["lng"]),
                            "controlPoint" => $point,
                            "punchTime" => 0,
                            "range" => $this->gpsErrorMargin,
                            "punched" => false,
                            "close" => false,
                        )
                    );
                    ++$point;
                    break;
                }
            }
        }

        $current = 0;
        $xmlTrack = $this->loadedGpx->trk->trkseg->trkpt;
        foreach ($xmlTrack as $place) {
            $coord = array((float)$place["lat"], (float)$place["lon"]);
            for ($i = 0; $i < count($controlPoints); $i++) {
                if (!$controlPoints[$i]["close"]) {
                    $range = $this->distanceBetween($coord, $controlPoints[$i]["coord"]);
                    if ($range <= $controlPoints[$i]["range"]) {
                        $controlPoints[$i]["range"] = $range;
                        $controlPoints[$i]["punchTime"] = $this->timeToMs((string)$place->time);
                        $controlPoints[$i]["punched"] = true;
                        if ($this->format === 0 and $i < $current and !$controlPoints[$i]["close"]) {
                            for ($j = $i; $j < count($controlPoints); $j++) {
                                $controlPoints[$j]["punchTime"] = 0;
                                $controlPoints[$j]["range"] = $this->gpsErrorMargin;
                                $controlPoints[$j]["punched"] = false;
                                $controlPoints[$j]["close"] = false;
                            }
                        }
                    }
                    if ($current !== $i and $range <= $this->gpsErrorMargin) {
                        if ($controlPoints[$current]["punched"]) {
                            $controlPoints[$current]["close"] = true;
                        }
                        $current = $i;
                    }
                }
            }
        }

        for ($i = 0; $i < count($controlPoints); $i++) {
            unset($controlPoints[$i]["coord"]);
            unset($controlPoints[$i]["range"]);
            unset($controlPoints[$i]["punched"]);
            unset($controlPoints[$i]["close"]);
            $controlPoints[$i] = json_decode(json_encode($controlPoints[$i]));
        }

        return $controlPoints;
    }

    public function timeToMs($formattedTime)
    {
        $timeInMs = 0;
        preg_match('/^(.+)\.(\d+)Z$/i', $formattedTime, $matches);
        if ($matches !== []) {
            $timeInMs = $matches[2];
        }
        $timeInMs += strtotime($formattedTime) * 1000;

        return $timeInMs;
    }

    public function msToTime($msTotal): string
    {
        $sec = (int)($msTotal / 1000);
        $ms = $msTotal - ($sec * 1000);
        $hrs = 0;
        $min = 0;

        if ($sec >= 3600) {
            $hrs = floor($sec / 3600);
            $sec = $sec % 3600;
        }
        if ($sec >= 60) {
            $min = floor($sec / 60);
            $sec = $sec % 60;
        }

        if (strlen((string)$hrs) === 1) {
            $hrs = "0".$hrs;
        }
        if (strlen((string)$min) === 1) {
            $min = "0".$min;
        }
        if (strlen((string)$sec) === 1) {
            $sec = "0".$sec;
        }
        if (strlen((string)$ms) === 1) {
            $ms = "0".$ms;
        }
        if (strlen((string)$ms) === 2) {
            $ms = "0".$ms;
        }

        return "1970-01-01T".$hrs.":".$min.":".$sec.".".$ms."Z";
    }

    public function distanceBetween($coord1, $coord2): float
    {
        return acos(sin(deg2rad($coord1[0])) * sin(deg2rad($coord2[0])) + cos(deg2rad($coord1[0])) * cos(deg2rad($coord2[0])) * cos(deg2rad($coord1[1]) - (deg2rad($coord2[1])))) * 6372795.477598;
    }

    public function loadGpx(): bool
    {
        try {
            $this->loadedGpx = simplexml_load_file($this->gpx->getPathname());

            return true;
        } catch (Exception $e) {
            $this->error = "invalid Gpx file";

            return false;
        }
    }

    public function convertGpxTime()
    {
        $firstTime = $this->timeToMs((string)$this->loadedGpx->trk->trkseg->trkpt[0]->time);

        for ($i = 0; $i < count($this->loadedGpx->trk->trkseg->trkpt); $i++) {
            $this->loadedGpx->trk->trkseg->trkpt[$i]->time = $this->msToTime($this->timeToMs($this->loadedGpx->trk->trkseg->trkpt[$i]->time) - $firstTime);
        }
    }

    public function removeTrackPointsBeforeStart($courseRepository)
    {
        $xmlRoute = simplexml_load_string($courseRepository->find($this->idCourse)->getXml());
        $start = array(
            "coord" => array((float)$xmlRoute->RaceCourseData->Control[0]->Position["lat"], (float)$xmlRoute->RaceCourseData->Control[0]->Position["lng"]),
            "controlPoint" => 0,
            "punchTime" => 0,
            "range" => $this->gpsErrorMargin,
            "punched" => false,
        );
        foreach ($this->loadedGpx->trk->trkseg->trkpt as $trackPoint) {
            $coord = array((float)$trackPoint["lat"], (float)$trackPoint["lon"]);
            $distance = $this->distanceBetween($coord, $start["coord"]);
            if ($distance <= $start["range"]) {
                $start["range"] = $distance;
                $start["punchTime"] = $this->timeToMs((string)$trackPoint->time);
            } elseif ($start["punched"] and $distance > $this->gpsErrorMargin) {
                break;
            }
        }
        $convertGpxTimeIsNeeded = false;
        $currentTime = 0;
        while ($currentTime !== $start["punchTime"]) {
            unset($this->loadedGpx->trk->trkseg->trkpt[0]);
            $currentTime = $this->timeToMs($this->loadedGpx->trk->trkseg->trkpt[0]->time);
            $convertGpxTimeIsNeeded = true;
        }
        if ($convertGpxTimeIsNeeded) {
            $this->convertGpxTime();
        }
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    public function getIdCourse(): int
    {
        return $this->idCourse;
    }

    public function setIdCourse($idCourse)
    {
        $this->idCourse = $idCourse;
    }

    public function getGpx(): UploadedFile
    {
        return $this->gpx;
    }

    public function setGpx($gpx)
    {
        $this->gpx = $gpx;
    }

    public function getFormat(): int
    {
        return $this->format;
    }

    public function setFormat($format)
    {
        $this->format = $format;
    }

    public function setLoadedGpx($loadedGpx)
    {
        $this->loadedGpx = $loadedGpx;
    }

    public function getError(): string
    {
        return $this->error;
    }
}