<?php

namespace App\Model;

use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\Persistence\ObjectManager;

class ModifyParticipantMakeEventCourse
{
    private ?Track $track = null;

    private ?ParticipantMakeEventCourse $participantMakeEventCourse;

    private ?int $participantId;

    private ?int $eventCourseId;

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getParticipantMakeEventCourse(): ?ParticipantMakeEventCourse
    {
        return $this->participantMakeEventCourse;
    }

    public function setParticipantMakeEventCourse(?ParticipantMakeEventCourse $participantMakeEventCourse): void
    {
        $this->participantMakeEventCourse = $participantMakeEventCourse;
    }

    public function getParticipantId(): ?int
    {
        return $this->participantId;
    }

    public function setParticipantId(?int $participantId): void
    {
        $this->participantId = $participantId;
    }

    public function getEventCourseId(): ?int
    {
        return $this->eventCourseId;
    }

    public function setEventCourseId(?int $eventCourseId): void
    {
        $this->eventCourseId = $eventCourseId;
    }

    public function modify(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourseRepository)
    {
        $this->participantMakeEventCourse->setTrack($this->track);
        $this->participantMakeEventCourse->setModified(true);
        $entityManager->persist($this->participantMakeEventCourse);
        $entityManager->flush();
        $this->participantMakeEventCourse->getEventCourse()->getEvent()->update($entityManager, $participantMakeEventCourseRepository);
    }
}