<?php

namespace App\Model;

use Doctrine\Persistence\ObjectManager;
use Exception;

use App\Entity\Track;
use function json_decode;

class CreateTrack
{
    private Track $track;
    private $data;

    public function __construct(string $data)
    {
        $this->track = new Track();
        $this->data = json_decode($data);
    }

    public function addTrack(ObjectManager $entityManager, $courseRepository): bool
    {
        if (!filter_var($this->data->courseId, FILTER_VALIDATE_INT)) {
            return false;
        }
        $course = $courseRepository->find(intval($this->data->courseId));
        if ($course == null) {
            return false;
        }
        // Keep this for compatibility purposes
        $pseudo = $this->data->orienteer;
        if ($pseudo == null) {
            $pseudo = "*****";
        }
        $this->track->setName($pseudo);
        $this->track->setTotalTime($this->data->totalTime);
        $format = $this->data->format;
        // Keep this for compatibility purposes
        if ($format == null) {
            $format = 0;
        }
        $this->track->setFormat($format);
        $this->track->setCourse($course);
        $this->track->setControlPoints($this->data->controlPoints);
        $this->track->setTrace($this->data->trace);
        try {
            $entityManager->persist($this->track);
            $entityManager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function getTrack(): Track
    {
        return $this->track;
    }
}