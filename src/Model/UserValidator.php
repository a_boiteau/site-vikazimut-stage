<?php

namespace App\Model;

class UserValidator
{
    static int $NO_ERROR = 0;
    static int $USERNAME_ERROR = 1;
    static int $EMAIL_ERROR = 2;

    public static function checkUsername(string $name): int
    {
        if (preg_match("/[a-zA-Z][a-zA-Z0-9]*/", $name)) {
            return self::$NO_ERROR;
        } else {
            return self::$USERNAME_ERROR;
        }
    }

    public static function checkEmail(string $email): int
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return self::$NO_ERROR;
        } else {
            return self::$EMAIL_ERROR;
        }
    }

    public static function checkValid(string $name, string $email)
    {
        // Caveat: smallest error code is always NO_ERR
        return max(self::checkUsername($name), self::checkEmail($email));
    }
}