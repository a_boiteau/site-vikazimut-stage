<?php

namespace App\Model;

use DOMDocument;
use ZipArchive;
use function date_default_timezone_set;

class FileUploader
{
    public static function uploadFile($file, int $userId)
    {
        $extension = strtolower($file->guessExtension());
        $isAcceptedFileExtension = ($extension == "xml" || $extension == "zip" || $extension == "kml" || $extension == "jpg" || $extension == "jpeg" || $extension == "png");
        if (!$isAcceptedFileExtension) {
            return false;
        }
        // KML file extension is .xml -> convert to .kml
        if ($extension == "xml") {
            $dom = new DOMDocument();
            $dom->load($file);
            $kmlTag = $dom->getElementsByTagName('kml');
            if ($kmlTag->length > 0) {
                $extension = "kml";
            }
        }
        self::cleanDir($userId, $extension);
        date_default_timezone_set("UTC");
        $newFileName = time().".".$userId.".";
        $tmpDir = "tmp_files";
        $file->move($tmpDir, $newFileName.$extension);

        // KMZ archive must be unzipped to retrieve .kml and .jpeg, then deleted
        if ($extension == "zip") {
            self::cleanDir($userId, "kml");
            self::cleanDir($userId, "jpg");
            $zip = new ZipArchive;
            $res = $zip->open($tmpDir.'/'.$newFileName.$extension);
            if ($res === true) {
                $zip->renameName("doc.kml", $newFileName."kml");
                $zip->renameName("files/tile_0_0.jpg", $newFileName."jpg");
                $zip->extractTo($tmpDir, array($newFileName."kml", $newFileName."jpg"));
                $zip->close();
            }
            if (file_exists($tmpDir."/".$newFileName.$extension)) {
                unlink($tmpDir."/".$newFileName.$extension);
            }
        }

        $content = "";
        if ($extension == "xml") {
            $xml = simplexml_load_file($tmpDir."/".$newFileName.$extension);
            $array = array();
            foreach ($xml->RaceCourseData->Course as $course) {
                array_push($array, $course->Name->__toString());
            }
            $content = $array;
        }

        return json_encode(["extension" => $extension, "content" => $content]);
    }

    /**
     * Directory will get clean only when one user adds a course
     * if you want to clean at regular interval you might want to use
     * a deamond calling this script (or another cleaning script).
     */
    public static function cleanDir(int $userid, string $extension)
    {
        date_default_timezone_set("UTC");
        $day_value = 24 * 60 * 60;
        $dir = "tmp_files";
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        foreach (scandir($dir) as $file) {
            if (!is_dir($dir."/".$file)) {
                $files_parts = preg_split("/[.]/", $file);
                //delete files older than one day
                if (intval($files_parts[0]) + $day_value < time()) {
                    //probably useless check but we never no
                    if (file_exists($dir."/".$file)) {
                        unlink($dir."/".$file);
                    }
                }
                //delete old file of user according to extension
                if ($userid == $files_parts[1]) {
                    if ($extension == $files_parts[2] || $extension == "all") {
                        //check in case the file was a day or more old
                        if (file_exists($dir."/".$file)) {
                            unlink($dir."/".$file);
                        }
                    }
                    if ($extension == "jpeg" || $extension == "jpg" || $extension == "png") {
                        if ($files_parts[2] == "jpeg" || $files_parts[2] == "jpg" || $files_parts[2] == "png") {
                            if (file_exists($dir."/".$file)) {
                                unlink($dir."/".$file);
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getFile(int $userid, string $extension): ?string
    {
        $dir = "tmp_files";
        foreach (scandir($dir) as $file) {
            if (!is_dir($dir."/".$file)) {
                $files_parts = preg_split("/[.]/", $file);
                if ($userid == $files_parts[1]) {
                    if ($extension == $files_parts[2] || ($extension == "image" && ($files_parts[2] == "png" || $files_parts[2] == "jpg" || $files_parts[2] == "jpeg"))) {
                        return $dir."/".$file;
                    }
                }
            }
        }

        return null;
    }
}
