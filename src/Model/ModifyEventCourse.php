<?php

namespace App\Model;

use App\Entity\EventCourse;

class ModifyEventCourse
{
    private ?int $id;
    private ?int $format;
    private ?int $missingPunchPenalty;
    private ?int $overTimePenalty;
    private string $time;

    function __construct(EventCourse $eventCourse)
    {
        $this->id = $eventCourse->getId();
        $this->format = $eventCourse->getFormat();
        $this->missingPunchPenalty = $eventCourse->getMissingPunchPenalty();
        $this->overTimePenalty = $eventCourse->getOverTimePenalty();
        $this->time = (string)$eventCourse->getMaxTime() / 1000;
    }

    function modify($eventCourseRepository, $entityManager)
    {
        $eventCourse = $eventCourseRepository->find($this->id);
        $eventCourse->setFormat($this->format);
        $eventCourse->setMissingPunchPenalty($this->missingPunchPenalty);
        $eventCourse->setOverTimePenalty($this->overTimePenalty);
        $eventCourse->setTime($this->time);
        $eventCourse->convertTimeToMaxTime();

        $entityManager->persist($eventCourse);
        $entityManager->flush();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getFormat(): ?int
    {
        return $this->format;
    }

    /**
     * @param int|null $format
     */
    public function setFormat(?int $format): void
    {
        $this->format = $format;
    }

    /**
     * @return int|null
     */
    public function getMissingPunchPenalty(): ?int
    {
        return $this->missingPunchPenalty;
    }

    /**
     * @param int|null $missingPunchPenalty
     */
    public function setMissingPunchPenalty(?int $missingPunchPenalty): void
    {
        $this->missingPunchPenalty = $missingPunchPenalty;
    }

    /**
     * @return int|null
     */
    public function getOverTimePenalty(): ?int
    {
        return $this->overTimePenalty;
    }

    /**
     * @param int|null $overTimePenalty
     */
    public function setOverTimePenalty(?int $overTimePenalty): void
    {
        $this->overTimePenalty = $overTimePenalty;
    }

    /**
     * @return float|int|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param float|int|string $time
     */
    public function setTime($time): void
    {
        $this->time = $time;
    }
}