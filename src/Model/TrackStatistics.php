<?php

namespace App\Model;

class TrackStatistics
{
    public function distanceInMeters($lat1, $lng1, $lat2, $lng2): float
    {
        $rad = M_PI / 180;
        $lat1r = $lat1 * $rad;
        $lat2r = $lat2 * $rad;
        $sinDLat = sin(($lat2 - $lat1) * $rad / 2);
        $sinDLon = sin(($lng2 - $lng1) * $rad / 2);
        $a = $sinDLat * $sinDLat + cos($lat1r) * cos($lat2r) * $sinDLon * $sinDLon;
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return 6371000.0 * $c;
    }

    public function computeSplitTime(array $controlPoints): array
    {
        $splitTimes = array();
        $splitTimes[] = "";
        $lastTime = $controlPoints[0]->punchTime + 1; // +1 to avoid == 0.
        for ($i = 1; $i < count($controlPoints); $i++) {
            $time = $controlPoints[$i]->punchTime;
            if ($lastTime > 0 && $time > 0) {
                $elapsedTime = $time - $lastTime;
                $splitTimes[] = $elapsedTime;
            } else {
                $splitTimes[] = 0;
            }
            $lastTime = $controlPoints[$i]->punchTime;
        }

        return $splitTimes;
    }

    public function computeActualSplitDistance(array $controlPoints, string $trace): array
    {
        $distances = array();
        $distances[] = "";
        $wayPoints = $this->parseXMLAsWayPoints($trace);

        $distance = 0;
        $currentLeg = 1;
        $previousWayPoint = $wayPoints[0];
        foreach ($wayPoints as $wayPoint) {
            $nextLegTimeInMillis = $controlPoints[$currentLeg]->punchTime;
            $currentWayPointTimeInMillis = $wayPoint[2];
            $distance += $this->distanceInMeters($previousWayPoint[0], $previousWayPoint[1], $wayPoint[0], $wayPoint[1]);
            $previousWayPoint = $wayPoint;
            if ($currentWayPointTimeInMillis >= $nextLegTimeInMillis and $currentLeg < count($controlPoints) - 1) {
                $currentLeg++;
                $distances[] = (int)$distance;
                $distance = 0;
            }
        }
        $distances[] = (int)$distance;

        return $distances;
    }

    public function computeSplitPaces(array $splitTimes, array $lengths): array
    {
        $paces = array();
        $paces[] = 0;
        for ($i = 1; $i < min(count($splitTimes), count($lengths)); $i++) {
            if (is_numeric($lengths[$i]) && intval($lengths[$i]) > 0) {
                $time = $splitTimes[$i];
                $length = intval($lengths[$i]);
                $pace = $time * 1000 / $length;
                $paces[] = $pace;
            } else {
                $paces[] = 0;
            }
        }

        return $paces;
    }

    // <trk><trkseg><trkpt lat=\"49.285667\" lon=\"-0.538659\"><time>1970-01-01T00:00:00.005Z</time></trkpt>
    private function parseXMLAsWayPoints(string $trace): array
    {
        $wayPoints = array();
        $xmlData = simplexml_load_string($trace);
        foreach ($xmlData->trk->trkseg->children() as $trkpt) {
            $timeInMs = $this->convertDateToMilliseconds($trkpt->time);
            $lat = $trkpt["lat"];
            $lon = $trkpt["lon"];
            array_push($wayPoints, [$lat, $lon, $timeInMs]);
        }

        return $wayPoints;
    }

    private function convertDateToMilliseconds(string $formattedTime): int
    {
        preg_match('/^(.+)\.(\d+)Z$/i', $formattedTime, $matches);
        $milliseconds = $matches[2];
        $timeInMs = strtotime($formattedTime);

        return $timeInMs * 1000 + $milliseconds;
    }

    public function computeLegLengths($simplexml): array
    {
        // 1. Creer la liste des balises.
        $root = $simplexml->RaceCourseData->Control;
        $controls = array();
        for ($i = 0; $i < $root->count(); $i++) {
            $controls[(string)$root[$i]->Id] = [$root[$i]->Position["lat"], $root[$i]->Position["lng"]];
        }

        // 2. Calculer les distances interpostes.
        $root = $simplexml->RaceCourseData;
        $lengths = array();
        $previousId = null;
        foreach ($root->Course->children() as $child) {
            if ($child->getName() === "CourseControl") {
                $id = (string)$child->Control;
                if ($previousId != null) {
                    $length = $this->distanceInMeters($controls[$previousId][0], $controls[$previousId][1], $controls[$id][0], $controls[$id][1]);
                    array_push($lengths, (int)$length);
                } else {
                    array_push($lengths, 0);
                }
                $previousId = $id;
            }
        }

        return $lengths;
    }
}