<?php

namespace App\Model;

use DateTime;
use Doctrine\Persistence\ObjectManager;
use DOMDocument;
use Exception;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\DBAL\DBALException;

use App\Entity\Course;
use App\Entity\User;
use function in_array;
use function simplexml_load_string;

class CreateCourse
{
    private Course $course;
    private User $user;
    private int $id;
    private $translator;
    private UrlGeneratorInterface $router;

    /**
     * @throws Exception
     */
    public function __construct($data, UrlGeneratorInterface $router, $repository)
    {
        $this->router = $router;
        $this->user = $data["user"];
        $this->translator = $data["translator"];
        $this->id = $data["id"];
        if ($this->id != -1) {
            $this->course = $repository->find($this->id);
            if (!(in_array($this->course, $this->user->getCourses()->toArray()) ||
                in_array("ROLE_ADMIN", $this->user->getRoles()))) {
                throw new Exception("access denied");
            }
        } else {
            $this->course = new Course();
        }
    }

    public function initView(): array
    {
        $data = array();
        $data["link"] = $this->router->generate("upload_course", ["id" => $this->id]);
        if ($this->id != -1) {
            $data["title"] = $this->translator->trans("course.modify");
            $data["modify"] = true;
            $data["name"] = $this->course->getName();
            $data["club"] = $this->course->getClub();
            $startDate = $this->course->getStartDate();
            $data["startDate"] = $startDate === null ? null : $startDate->format('Y-m-d H:i:s');
            $endDate = $this->course->getEndDate();
            $data["endDate"] = $endDate === null ? null : $endDate->format('Y-m-d H:i:s');
            $data["isPrintable"] = $this->course->getPrintable();
        } else {
            $data["title"] = $this->translator->trans("course.add");
            $data["modify"] = false;
            $data["startDate"] = null;
            $data["endDate"] = null;
            $data["isPrintable"] = false;
        }

        return $data;
    }

    private function xmlCourseFilter(string $xmlPath, int $idSelectedCourse)
    {
        $error = CourseValidator::checkXml(file_get_contents($xmlPath));
        if ($error == CourseValidator::$NO_ERROR) {
            $domDocument = new DOMDocument();
            $domDocument->load($xmlPath);
            $domNodeList = $domDocument->getElementsByTagname('Course');
            $nodeNumber = $domNodeList->length;
            if ($nodeNumber == 1) {
                return $xmlPath;
            }
            if ($nodeNumber > 1) {
                for ($i = $nodeNumber - 1; $i >= 0; $i--) {
                    $domNode = $domNodeList->item($i);
                    if ($i != $idSelectedCourse) {
                        $domNode->parentNode->removeChild($domNode);
                    }
                }
                $files_parts = preg_split("/[.]/", $xmlPath);
                $xmlPath = $files_parts[0]."modified".".".$files_parts[1].".".$files_parts[2];
                file_put_contents($xmlPath, $domDocument->saveXML());

                return $xmlPath;
            }

            return CourseValidator::$ERROR_XML_INVALID;
        } else {
            return $this->translateError($error);
        }
    }

    private function generatePreviewXml1(string $xml)
    {
        $error = CourseValidator::checkXml($xml);
        if ($error == CourseValidator::$NO_ERROR) {
            $data = [];
            $sxml = simplexml_load_string($xml);
            $xmlData = $sxml->RaceCourseData;
            foreach ($xmlData->Course->children() as $children) {
                if ($children->getName() === "CourseControl") {
                    $data[] = [(string)$children->Control, (string)$children["type"]];
                }
            }

            return $data;
        } else {
            return $this->translateError($error);
        }
    }

    private function generatePreviewXml2(string $xml)
    {
        $error = CourseValidator::checkXml($xml);
        if ($error == CourseValidator::$NO_ERROR) {
            $data = array();
            $sxml = simplexml_load_string($xml);
            foreach ($sxml->RaceCourseData->children() as $children) {
                if ($children->getName() === "Control") {
                    $data[(string)$children->Id] = array((string)$children->Position["lat"], (string)$children->Position["lng"]);
                }
            }

            return $data;
        } else {
            return $this->translateError($error);
        }
    }

    private function generatePreviewKml(string $kml): string
    {
        $error = CourseValidator::checkKml($kml);
        if ($error == CourseValidator::$NO_ERROR) {
            return $kml;
        } else {
            return $this->translateError($error);
        }
    }

    private function generatePreviewImage(string $image): string
    {
        $error = CourseValidator::checkImage($image);
        if ($error == CourseValidator::$NO_ERROR) {
            return $image;
        } else {
            return $this->translateError($error);
        }
    }

    public function preview(Request $request): array
    {
        $data = array();
        $post = $request->request;
        if ($post->get("xml") == "true") {
            $xmlPath = FileUploader::getFile($this->user->getId(), "xml");
            $xmlPath = $this->xmlCourseFilter($xmlPath, $post->get("selectedXmlCourse"));
            $xml = file_get_contents($xmlPath);
            $data["xml1"] = $this->generatePreviewXml1($xml);
            $data["xml2"] = $this->generatePreviewXml2($xml);
        } elseif ($this->course->getXml() != null) {
            $data["xml1"] = $this->generatePreviewXml1($this->course->getXml());
            $data["xml2"] = $this->generatePreviewXml2($this->course->getXml());
        } else {
            $data["xml1"] = $this->translator->trans("no.xml.submitted");
            $data["xml2"] = $this->translator->trans("no.xml.submitted");
        }

        if ($post->get("kml") == "true") {
            $kml = file_get_contents(FileUploader::getFile($this->user->getId(), "kml"));
            $data["kml"] = $this->generatePreviewKml($kml);
        } elseif ($this->course->getKml() != null) {
            $data["kml"] = $this->generatePreviewKml($this->course->getKml());
        } else {
            $data["kml"] = $this->translator->trans("no.kml.submitted");
        }

        if ($post->get("image") == "true") {
            $image = FileUploader::getFile($this->user->getId(), "image");
            $data["image"] = $this->generatePreviewImage($image);
        } elseif ($this->course->getImage() != null) {
            $data["image"] = $this->generatePreviewImage($this->course->getImage());
        } else {
            $data["image"] = $this->translator->trans("no.image.submitted");
        }

        return $data;
    }

    public function processData(Request $request, ObjectManager $entityManager): array
    {
        $post = $request->request;
        $data = array();
        // all following fields are null if not submitted
        $xml = FileUploader::getFile($this->user->getId(), "xml");
        $kml = FileUploader::getFile($this->user->getId(), "kml");
        $image = FileUploader::getFile($this->user->getId(), "image");

        $name = $post->get("name");
        $startingTime = $post->get("startingTime");
        $endingTime = $post->get("endingTime");
        $printable = $post->get("printable");
        $club = $post->get("club");
        $this->course->setUpdateAt(new DateTime('NOW'));
        if ($this->id == -1) {
            if ($name == null) {
                $data["name"] = $this->translator->trans("no.name.submitted");
            }
            if ($xml == null) {
                $data["xml"] = $this->translator->trans("no.xml.submitted");
            }
            if ($kml == null) {
                $data["kml"] = $this->translator->trans("no.kml.submitted");
            }
            if ($image == null) {
                $data["image"] = $this->translator->trans("no.image.submitted");
            }
            if ($xml == null || $kml == null || $image == null) {
                return $data;
            }
            $this->course->setName($name);
            $this->course->setClub($club);
            $xml = $this->xmlCourseFilter($xml, $post->get("selectedXmlCourse"));
            $this->course->setXml(file_get_contents($xml));
            $this->course->setKml(file_get_contents($kml));
            $this->course->setImage($image);
            $this->course->setCreator($this->user);
            [$latitude, $longitude, $length] = $this->extractLocationAndLengthFromXmlFile($xml);
            if ($latitude == null || $longitude == null || $length == null) {
                $data["error"] = 'Erreur : Mauvais fichier xml. Il manque la géolocalisation de la carte (latitude, longitude) dans le fichier xml';

                return $data;
            }
            try {
                $startingTime !== " :00" ? $this->course->setStartDate(new DateTime($startingTime)) : $this->course->setStartDate(null);
            } catch (Exception $e) {
                $data["error"] = 'Erreur : contrainte horaire incompatible';
            }
            try {
                $endingTime !== " :00" ? $this->course->setEndDate(new DateTime($endingTime)) : $this->course->setEndDate(null);
            } catch (Exception $e) {
                $data["error"] = 'Erreur : contrainte horaire incompatible';
            }
            $this->course->setLatitude($latitude);
            $this->course->setLongitude($longitude);
            $this->course->setLength($length);
            $this->course->setPrintable($printable);
            if (!$this->course->isValid()) {
                $data["error"] = $this->translateError($this->course->getError());

                return $data;
            } else {
                //need to save the course first to have an id
                try {
                    $entityManager->persist($this->course);
                    $entityManager->flush();
                } catch (Exception $e) {
                    $data["error"] = $e->getMessage();

                    return $data;
                }
            }
        } else {
            if ($name != null) {
                $this->course->setName($name);
            }
            if ($club != null) {
                $this->course->setClub($club);
            }
            if ($xml != null) {
                $xml = $this->xmlCourseFilter($xml, $post->get("selectedXmlCourse"));
                $this->course->setXml(file_get_contents($xml));
                [$latitude, $longitude, $length] = $this->extractLocationAndLengthFromXmlFile($xml);
                if ($latitude == null || $longitude == null || $length == null) {
                    $data["error"] = 'Erreur : Mauvais fichier xml. Il manque la géolocalisation de la carte (latitude, longitude) dans le fichier xml';

                    return $data;
                }
                $this->course->setLatitude($latitude);
                $this->course->setLongitude($longitude);
                $this->course->setLength($length);
            }
            if ($kml != null) {
                $this->course->setKml(file_get_contents($kml));
            }
            try {
                $startingTime !== " :00" ? $this->course->setStartDate(new DateTime($startingTime)) : $this->course->setStartDate(null);
            } catch (Exception $e) {
                $data["error"] = 'Erreur : contrainte horaire incompatible';
            }
            try {
                $endingTime !== " :00" ? $this->course->setEndDate(new DateTime($endingTime)) : $this->course->setEndDate(null);
            } catch (Exception $e) {
                $data["error"] = 'Erreur : contrainte horaire incompatible';
            }
            if ($printable != null) {
                $this->course->setPrintable($printable);
            }
            if (!$this->course->isValid()) {
                $data["error"] = $this->translateError($this->course->getError());

                return $data;
            }
        }

        if ($post->get("image") == "true") {
            $image = FileUploader::getFile($this->user->getId(), "image");
            rename($image, "upload/".$this->course->getId().".".pathinfo($image)["extension"]);
            $this->course->setImage("upload/".$this->course->getId().".".pathinfo($image)["extension"]);
        }

        try {
            $entityManager->persist($this->course);
            $entityManager->flush();
        } catch (DBALException $e) {
            if (strpos($e->getMessage(), "Duplicate entry")) {
                $data["error"] = $this->translator->trans("name.already.exist");
            } elseif (strpos($e->getMessage(), "Integrity constraint violation")) {
                $data["error"] = $this->translator->trans("error.incorrect_data");
            } else {
                $data["error"] = $e->getMessage();
            }

            return $data;
        } catch (Exception $e) {
            $data["error"] = $e->getMessage();

            return $data;
        }
        FileUploader::cleanDir($this->user->getId(), "all");
        $data["route"] = $this->router->generate("preview_course", ["id" => $this->course->getId()]);

        return $data;
    }

    function extractLocationAndLengthFromXmlFile(string $xmlFilename): array
    {
        $contents = simplexml_load_file($xmlFilename);
        $len = $contents->RaceCourseData->Course[0]->Length;
        $firstControlPointName = $this->getFirstControlPointName($contents);
        $controlPoint = $this->getControlPointFromName($contents, $firstControlPointName);
        if ($controlPoint == null) {
            return [null, null, null];
        }
        $lat = $controlPoint->Position['lat'];
        $lng = $controlPoint->Position['lng'];

        return [$lat, $lng, (int)$len];
    }

    public static function getFirstControlPointName(?SimpleXMLElement $xmlContents): ?string
    {
        foreach ($xmlContents->RaceCourseData->Course->CourseControl as $children) {
            if (strcasecmp("Start", (string)$children["type"]) === 0) {
                return $children->Control;
            }
        }

        return null;
    }

    public static function getControlPointFromName($xmlContents, string $controlPointName): ?SimpleXMLElement
    {
        foreach ($xmlContents->RaceCourseData->Control as $control) {
            if (strcasecmp($control->Id, $controlPointName) === 0) {
                return $control;
            }
        }

        return null;
    }

    public function translateError(int $error_code): string
    {
        switch ($error_code) {
            case CourseValidator::$NO_ERROR:
                return $this->translator->trans("error.no");
            case CourseValidator::$ERROR_COURSE_NAME_INVALID:
                return $this->translator->trans("error.course.name.invalid");
            case CourseValidator::$ERROR_COURSE_CLUB_NAME_INVALID:
                return $this->translator->trans("error.course.club.name.invalid");
            case CourseValidator::$ERROR_IMAGE_NOT_FOUND;
                return $this->translator->trans("error.image.does.not.exist");
            case CourseValidator::$ERROR_IMAGE_TYPE_INVALID;
                return $this->translator->trans("error.image.invalide.only.jpg.png.accepted");
            case CourseValidator::$ERROR_XML_EMPTY:
                return $this->translator->trans("error.xml.empty");
            case CourseValidator::$ERROR_XML_INVALID:
                return $this->translator->trans("error.xml.structure.invalid");
            case CourseValidator::$ERROR_XML_INVALID_CONTENTS:
                return $this->translator->trans("error.xml.position.invalid");
            case CourseValidator::$ERROR_XML_NO_COURSE:
                return $this->translator->trans("error.xml.no.course.defined");
            case CourseValidator::$ERROR_XML_NO_START_END:
                return $this->translator->trans("error.xml.no.start.end");
            case CourseValidator::$ERROR_XML_INVALID_CONTROL_POINT:
                return $this->translator->trans("error.xml.invalid.control_point");
            case CourseValidator::$ERROR_KML_EMPTY:
                return $this->translator->trans("error.kml.empty");
            case CourseValidator::$ERROR_KML_INVALID:
                return $this->translator->trans("error.kml.structure.invalid");
            case CourseValidator::$ERROR_KML_INVALID_POS:
                return $this->translator->trans("error.kml.position.invalid");
            case CourseValidator::$ERROR_COURSE_PRINTABLE_INVALID :
                return $this->translator->trans("error.printable.choice.invalid");
            default: //unknow error
                return $this->translator->trans("error.unknown");
        }
    }
}