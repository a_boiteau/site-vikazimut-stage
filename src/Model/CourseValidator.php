<?php

namespace App\Model;

use DOMDocument;
use function libxml_use_internal_errors;
use function simplexml_load_string;

class CourseValidator
{
    //outside of class compare to those variable
    static int $NO_ERROR = 0;
    static int $ERROR_COURSE_NAME_INVALID = 1;
    static int $ERROR_IMAGE_NOT_FOUND = 2;
    static int $ERROR_IMAGE_TYPE_INVALID = 3;
    static int $ERROR_COURSE_PRINTABLE_INVALID = 4;
    static int $ERROR_COURSE_CLUB_NAME_INVALID = 5;
    static int $ERROR_XML_EMPTY = 10;
    static int $ERROR_XML_INVALID = 11;
    static int $ERROR_XML_INVALID_CONTENTS = 12;
    static int $ERROR_XML_NO_COURSE = 13;
    static int $ERROR_XML_NO_START_END = 14;
    static int $ERROR_XML_INVALID_CONTROL_POINT = 15;
    static int $ERROR_KML_EMPTY = 20;
    static int $ERROR_KML_INVALID = 21;
    static int $ERROR_KML_INVALID_POS = 22;

    public static function checkImage(string $image): int
    {
        if (!file_exists($image)) {
            return self::$ERROR_IMAGE_NOT_FOUND;
        }
        $extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));
        if (!($extension == "png" || $extension == "jpg" || $extension == "jpeg")) {
            return self::$ERROR_IMAGE_TYPE_INVALID;
        }

        return self::$NO_ERROR;
    }

    public static function checkXml(string $xml): int
    {
        if ($xml == "") {
            return self::$ERROR_XML_EMPTY;
        }
        //allow to "catch" the error
        libxml_use_internal_errors(true);
        $sxml = simplexml_load_string($xml);
        if (!$sxml) {
            return self::$ERROR_XML_INVALID;
        }
        $xmlData = $sxml->RaceCourseData;
        if (!$xmlData) {
            return self::$ERROR_XML_INVALID;
        }
        if (!$xmlData->Map || !$xmlData->Map->MapPositionTopLeft || !$xmlData->Map->MapPositionBottomRight) {
            return self::$ERROR_XML_INVALID_CONTENTS;
        }
        $start = false;
        $finish = false;
        $controlPointName = [];
        if (!$xmlData->Course) {
            return self::$ERROR_XML_NO_COURSE;
        }
        foreach ($xmlData->Course->children() as $children) {
            if ($children->getName() === "CourseControl") {
                if ((string)$children["type"] === "Start") {
                    $start = true;
                } elseif ((string)$children["type"] === "Finish") {
                    $finish = true;
                }
                array_push($controlPointName, $children->Control);
            }
        }
        if (!($start && $finish)) {
            return self::$ERROR_XML_NO_START_END;
        }
        foreach ($xmlData->children() as $children) {
            if ($children->getName() === "Control") {
                $baliseDelete = array((string)$children->Id);
                $controlPointName = array_diff($controlPointName, $baliseDelete);
            }
        }
        if (count($controlPointName) > 0) {
            return self::$ERROR_XML_INVALID_CONTROL_POINT;
        }

        return self::$NO_ERROR;
    }

    public static function checkKml(string $fileName): int
    {
        if ($fileName == "") {
            return self::$ERROR_KML_EMPTY;
        }
        $dom = new DOMDocument();
        $dom->loadXML($fileName);
        $kmlTag = $dom->getElementsByTagName('kml');
        if ($kmlTag->length == 0) {
            return self::$ERROR_KML_INVALID;
        }
        $northTag = $dom->getElementsByTagName('north');
        $eastTag = $dom->getElementsByTagName('east');
        $southTag = $dom->getElementsByTagName('south');
        $westTag = $dom->getElementsByTagName('west');
        if ($northTag->length == 0 || $southTag->length == 0 || $eastTag->length == 0 || $westTag->length == 0) {
            return self::$ERROR_KML_INVALID_POS;
        }

        return self::$NO_ERROR;
    }

    public static function checkPrintable(bool $printable): int
    {
        if ($printable !== true && $printable !== false) {
            return self::$ERROR_COURSE_PRINTABLE_INVALID;
        }

        return self::$NO_ERROR;
    }

    public static function checkValid(string $xml, string $image, string $kml, bool $printable)
    {
        //smallest error code is always NO_ERROR
        return max(
            self::checkXml($xml),
            self::checkImage($image),
            self::checkKml($kml),
            self::checkPrintable($printable)
        );
    }
}
