<?php

namespace App\Model;

use App\Entity\EventCourse;
use App\Entity\Participant;
use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\Persistence\ObjectManager;

class ManuallySetPenaltyOfParticipantMakeEventCourse
{
    private ?Participant $participant = null;
    private ?EventCourse $eventCourse = null;
    private ?int $nbPM = null;
    private ?int $nbOT = null;

    /**
     * @return Participant|null
     */
    public function getParticipant(): ?Participant
    {
        return $this->participant;
    }

    /**
     * @param Participant|null $participant
     */
    public function setParticipant(?Participant $participant): void
    {
        $this->participant = $participant;
    }

    /**
     * @return EventCourse|null
     */
    public function getEventCourse(): ?EventCourse
    {
        return $this->eventCourse;
    }

    /**
     * @param EventCourse|null $eventCourse
     */
    public function setEventCourse(?EventCourse $eventCourse): void
    {
        $this->eventCourse = $eventCourse;
    }

    /**
     * @return int|null
     */
    public function getNbPM(): ?int
    {
        return $this->nbPM;
    }

    /**
     * @param int|null $nbPM
     */
    public function setNbPM(?int $nbPM): void
    {
        $this->nbPM = $nbPM;
    }

    /**
     * @return int|null
     */
    public function getNbOT(): ?int
    {
        return $this->nbOT;
    }

    /**
     * @param int|null $nbOT
     */
    public function setNbOT(?int $nbOT): void
    {
        $this->nbOT = $nbOT;
    }

    public function modify(ObjectManager $entityManager, ParticipantMakeEventCourseRepository $participantMakeEventCourseRepository)
    {
        $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $this->eventCourse, "participant" => $this->participant));
        if ($this->nbOT === null) {
            $participantMakeEventCourse->setOtPenaltyManuallySet(false);
        } else {
            $participantMakeEventCourse->setOtPenaltyManuallySet(true);
            $participantMakeEventCourse->setNbOverTimePenalty($this->nbOT);
        }
        if ($this->nbPM === null) {
            $participantMakeEventCourse->setPmPenaltyManuallySet(false);
        } else {
            $participantMakeEventCourse->setPmPenaltyManuallySet(true);
            $participantMakeEventCourse->setNbMissingPunchPenalty($this->nbPM);
        }
        $entityManager->persist($participantMakeEventCourse);
        $entityManager->flush();
        $participantMakeEventCourse->getEventCourse()->getEvent()->update($entityManager, $participantMakeEventCourseRepository);
    }
}
