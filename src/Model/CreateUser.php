<?php

namespace App\Model;

use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use PDOException;

use App\Entity\User;
use Symfony\Contracts\Translation\TranslatorInterface;

class CreateUser
{
    private User $user;
    private TranslatorInterface $translator;
    private string $plainPassword;

    public function __construct($user, $translator)
    {
        $this->user = $user;
        $this->translator = $translator;
    }

    public function addUser(UserPasswordHasherInterface $passwordEncoder, ObjectManager $entityManager)
    {
        if ($this->user->getPassword() != "") {
            $info = password_get_info($this->user->getPassword());
            if ($info['algo'] == 0) {
                if (strlen($this->user->getPassword()) < 4 || strlen($this->user->getPassword()) > 4095) {
                    return $this->translator->trans("password.invalid_length", ['length' => 4]);
                } else {
                    $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->user->getPassword());
                    $this->user->setPassword($encodedPassword);
                }
            }
        } else {
            $this->plainPassword = self::generatePassword();
            $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->plainPassword);
            $this->user->setPassword($encodedPassword);
        }
        if (!$this->user->isValid()) {
            return $this->translateError($this->user->getError());
        } else {
            try {
                $entityManager->persist($this->user);
                $entityManager->flush();
            } catch (DBALException $e) {
                if (strpos($e->getMessage(), "Duplicate entry")) {
                    return $this->translator->trans("name.already.exist");
                } elseif (strpos($e->getMessage(), "Integrity constraint violation")) {
                    return $this->translator->trans("error.incorrect_data");
                } else {
                    return $e->getMessage();
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }

            return true;
        }
    }

    public function changeUser(UserPasswordHasherInterface $passwordEncoder, ObjectManager $entityManager)
    {
        if ($this->user->getPlainPassword() != '') {
            $encodedPassword = $passwordEncoder->hashPassword($this->user, $this->user->getPlainPassword());
            $this->user->setPassword($encodedPassword);
        }
        if (!$this->user->isValid()) {
            return $this->translateError($this->user->getError());
        } else {
            $entityManager->persist($this->user);
            $entityManager->flush();

            try {
                $entityManager->persist($this->user);
                $entityManager->flush();
            } catch (PDOException $e) {
                return $e->getMessage();
            }

            return true;
        }
    }

    public function forgotPassword(UserPasswordHasherInterface $encoder, ObjectManager $entityManager)
    {
        $this->plainPassword = self::generatePassword();
        $encoded = $encoder->hashPassword($this->user, $this->plainPassword);
        $this->user->setPassword($encoded);
        $entityManager->persist($this->user);
        $entityManager->flush();
    }

    public function changePassword(String $newPassword, UserPasswordHasherInterface $encoder, ObjectManager $entityManager)
    {
        $encoded = $encoder->hashPassword($this->user, $newPassword);
        $this->user->setPassword($encoded);
        $entityManager->persist($this->user);
        $entityManager->flush();
    }

    public function sendEmail(Swift_Mailer $mailer, $new,TranslatorInterface $translator)
    {
        if ($new) {
            $body = $translator->trans('mailer.hello') . ",\n" . $translator->trans('mailer.your.connection.data') . ": \n";
            $body .= $translator->trans('mailer.username') . " : ".$this->user->getUsername()."\n";
        } else {
            $body = $translator->trans('mailer.hello') . " " . $this->user->getUsername()."\n";
            $body .= $translator->trans('mailer.password.reset') . ".\n";
        }
        $body .= $translator->trans('mailer.temporary.password') . " : " . $this->plainPassword."\n\n";
        $body .= $translator->trans('mailer.no.reply') . ".\n";
        $message = (new Swift_Message("Vikazimut"))
            ->setFrom("noreply@vikazimut.vikazim.fr")
            ->setTo($this->user->getEmail())
            ->setBody($body);
        $mailer->send($message);
    }

    public static function generatePassword(): string
    {
        // Random_int is supposed to be "cryptographically secure" number
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $max = strlen($chars) - 1;
        $str = '';
        for ($i = 0; $i < 8; ++$i) {
            try {
                $str .= $chars[random_int(0, $max)];
            } catch (Exception $exception) {
                $str .= $chars[mt_rand(0, $max)];
            }
        }

        return $str;
    }

    public function translateError(int $error_code): string
    {
        switch ($error_code) {
            case UserValidator::$NO_ERROR:
                return $this->translator->trans("error.no");
            case UserValidator::$USERNAME_ERROR:
                return $this->translator->trans("error.user.name.invalid");
            case UserValidator::$EMAIL_ERROR;
                return $this->translator->trans("error.email.invalid");
            default: // unknown error
                return $this->translator->trans("error.unknown");
        }
    }
}
