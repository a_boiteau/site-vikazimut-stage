<?php

namespace App\Model;

use Doctrine\Persistence\ObjectManager;
use Exception;

use App\Entity\MissingControlPoint;
use function json_decode;

class CreateMissingControlPoint
{
    private MissingControlPoint $controlPoint;
    private $data;

    public function __construct(string $data)
    {
        $this->controlPoint = new MissingControlPoint();
        $this->data = json_decode($data);
    }

    public function addControlPoint(ObjectManager $entityManager, $repository): bool
    {
        if (filter_var($this->data->courseId, FILTER_VALIDATE_INT)
            && ($this->data->controlPointId != null)) {
            $course = $repository->find(intval($this->data->courseId));
            if ($course == null) {
                return false;
            }
        } else {
            return false;
        }
        $this->controlPoint->setControlPointId($this->data->controlPointId);
        $this->controlPoint->setCourse($course);
        $entity = $entityManager->getRepository(MissingControlPoint::class)->findOneBy(
            array(
                'controlPointId' => $this->data->controlPointId,
                'courseId' => $this->controlPoint->getCourse()->getId(),
            )
        );
        $isDuplicated = !empty($entity);
        if ($isDuplicated) {
            return true;
        }

        try {
            $entityManager->persist($this->controlPoint);
            $entityManager->flush();
        } catch (Exception $e) {

            return false;
        }

        return true;
    }
}
