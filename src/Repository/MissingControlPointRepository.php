<?php

namespace App\Repository;

use App\Entity\MissingControlPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MissingControlPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissingControlPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissingControlPoint[]    findAll()
 * @method MissingControlPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissingControlPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissingControlPoint::class);
    }
}
