<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ChangeLocaleController extends AbstractController
{
    /**
     * @Route("/change_locale/{locale}", name="change_locale")
     */
    public function changeLocale($locale, Request $request): RedirectResponse
    {
        $newUrl = explode('/', $request->headers->get('referer'));
        $prefixToBeRemoved = ($request->getLocale() === "fr") ? 0 : 1;
        if ($locale === "fr") {
            array_splice($newUrl, 3, $prefixToBeRemoved);
        } else {
            array_splice($newUrl, 3, $prefixToBeRemoved, $locale);
        }
        $newUrl = implode("/", $newUrl);

        return $this->redirect($newUrl);
    }
}
