<?php

namespace App\Controller;

use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use App\Form\PlannerType;
use App\Entity\User;
use App\Model\CreateUser;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/show_all", name="all_planners")
     */
    public function show_all(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAllPlanner();

        return $this->render(
            'admin/show_all.html.twig',
            [
                'users' => $users,
            ]
        );
    }

    /**
     * @Route("/admin/delete_user/{id}", name="delete_user")
     */
    public function delete(int $id, TranslatorInterface $translator): RedirectResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user->remove($this->getDoctrine()->getManager());

        $message = $translator->trans("deletion.succeeded");
        return $this->redirectToRoute('all_planners', ['message' => $message . ' !']);
    }

    /**
     * @Route("/admin/add_planner", name="add_planner")
     */
    public function add_user(Request $request, TranslatorInterface $translator, UserPasswordHasherInterface $encoder, Swift_Mailer $mailer): Response
    {
        $planner = new User();
        $formBuilder = $this->createForm(PlannerType::class, $planner, ['password' => false]);
        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            $user = $formBuilder->getData();
            $creator = new CreateUser($user, $translator);
            $valid = $creator->addUser($encoder, $this->getDoctrine()->getManager());
            if ($valid === true) {
                $creator->sendEmail($mailer, true, $translator);

                return $this->redirectToRoute('all_planners');
            } else {
                return $this->render(
                    'admin/add_planner.html.twig',
                    [
                        'form' => $formBuilder->createView(),
                        'error' => $valid,
                    ]
                );
            }
        }

        return $this->render(
            'admin/add_planner.html.twig',
            [
                'form' => $formBuilder->createView(),
            ]
        );
    }

    /**
     * @Route("/admin/change_planner_password/{id}", name="change_planner_password")
     */
    public function password_modification(int $id, UserPasswordHasherInterface $encoder, TranslatorInterface $translator, Swift_Mailer $mailer): RedirectResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $creator = new CreateUser($user, $translator);
        $creator->forgotPassword($encoder, $this->getDoctrine()->getManager());
        $creator->sendEmail($mailer, false,$translator); //the user is notified of the password change

        $message = $translator->trans("password.reset.succeeded");
        return $this->redirectToRoute('all_planners', ['message' => $message . ' !']);
    }
}