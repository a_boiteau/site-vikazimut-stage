<?php

namespace App\Controller;

use LogicException;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use App\Entity\User;
use App\Model\CreateUser;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/reset-password", name="reset_password")
     */
    public function reset_password(Request $request, TranslatorInterface $translator, UserPasswordHasherInterface $encoder, Swift_Mailer $mailer)
    {
        $form = $this->createFormBuilder()
            ->add('username', TextType::class, ['label' => 'user.identifier'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $username = $form->getData()["username"];
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $username]);
            if ($user) {
                $creator = new CreateUser($user, $translator);
                $creator->forgotPassword($encoder, $this->getDoctrine()->getManager());
                $creator->sendEmail($mailer, false, $translator);

                return $this->redirectToRoute('all_planners');
            } else {
                return $this->render(
                    'security/reset_password.html.twig',
                    [
                        'form' => $form->createView(),
                        'error' => "Identifiant inconnu",
                    ]
                );
            }
        }

        return $this->render(
            'security/reset_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
