<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use App\Form\PlannerType;
use App\Entity\User;
use App\Model\CreateUser;
use function in_array;

class ProfileController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/profile/{id}", name="profile", defaults={"id": -1}, requirements={"id"="\d+"})
     */
    public function index(int $id): Response
    {
        $user = $this->security->getUser();
        if ($id != -1 && in_array('ROLE_ADMIN', $user->getRoles())) {
            $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        }

        return $this->render(
            'profile/index.html.twig',
            [
                'user' => $user,
            ]
        );
    }

    /**
     * @Route("/profile/modify_planner_info", name="modify_planner_info")
     */
    public function modify_planner_info(Request $request, TranslatorInterface $translator, UserPasswordHasherInterface $passwordEncoder): Response
    {
        $planner = $this->security->getUser();
        $formBuilder = $this->createForm(PlannerType::class, $planner, ['password' => true]);
        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            $user = $formBuilder->getData();
            $creator = new CreateUser($user, $translator);
            $valid = $creator->changeUser($passwordEncoder, $this->getDoctrine()->getManager());
            if ($valid === true) {
                return $this->redirectToRoute('profile');
            } else {
                return $this->render(
                    'profile/modify_planner_info.html.twig',
                    [
                        'form' => $formBuilder->createView(),
                        'error' => $valid,
                    ]
                );
            }
        }

        return $this->render(
            'profile/modify_planner_info.html.twig',
            [
                'form' => $formBuilder->createView(),
            ]
        );
    }
}
