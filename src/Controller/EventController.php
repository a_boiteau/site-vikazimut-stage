<?php

namespace App\Controller;

use App\Entity\EventCourse;
use App\Entity\Event;
use App\Entity\Participant;
use App\Entity\ParticipantMakeEventCourse;
use App\Entity\User;
use App\Form\EventType;
use App\Form\EventCourseType;
use App\Form\ManuallySetPenaltyOfParticipantMakeEventCourseType;
use App\Form\ModifyEventCourseType;
use App\Form\ModifyParticipantMakeEventCourseType;
use App\Form\ParticipantType;
use App\Model\ManuallySetPenaltyOfParticipantMakeEventCourse;
use App\Model\ModifyEventCourse;
use App\Model\ModifyParticipantMakeEventCourse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class EventController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/my_event/show_all/{id}", name="my_events", defaults={"id": -1})
     * @param int $id
     * @return Response
     */
    public function show_all(int $id): Response
    {
        if ($id == -1) {
            $user = $this->security->getUser();
        } else {
            if (in_array('ROLE_ADMIN', $this->security->getUser()->getRoles())) {
                $user = $this->getDoctrine()->getRepository(User::class)->find($id);
            } else {
                throw new AccessDeniedException();
            }
        }

        return $this->render('event/my_events.html.twig',
            [
                'events' => $user->getEvents(),
            ]
        );
    }

    /**
     * @Route("/my_event/create", name="create_my_event")
     * @param Request $request
     * @return Response
     */
    public function create_event(Request $request): Response
    {
        $event = new Event();
        $event->setCreator($this->security->getUser());

        $eventForm = $this->createForm(EventType::class, $event);
        $eventForm->handleRequest($request);

        if ($eventForm->isSubmitted() && $eventForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();
            return $this->redirectToRoute('my_events');
        }

        return $this->render(
            'event/my_event_create.html.twig',
            [
                'eventForm' => $eventForm->createView(),
            ]
        );
    }

    /**
     * @Route("/my_event/details/{id}", name="my_event_details")
     */
    public function details(int $id, Request $request): Response
    {
        $user = $this->security->getUser();
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        $formsView = [];
        $participants = $event->getParticipants();
        $eventCourses = $event->getEventCourses();
        $participantMakeEventCourseRepository = $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class);
        for ($i = 0; $i < sizeof($participants); $i++) {
            $formsView[$i] = array();
            for ($j = 0; $j < sizeof($eventCourses); $j++) {
                $participantMakeEventCourse = $participantMakeEventCourseRepository->find(array("eventCourse" => $eventCourses->get($j), "participant" => $participants->get($i)));
                $modifyParticipantMakeEventCourse = new ModifyParticipantMakeEventCourse();
                $modifyParticipantMakeEventCourse->setParticipantMakeEventCourse($participantMakeEventCourse);
                $modifyParticipantMakeEventCourseForm = $this->createForm(ModifyParticipantMakeEventCourseType::class, $modifyParticipantMakeEventCourse, ['course' => $eventCourses->get($j)->getCourse(), 'participant' => $participants->get($i)->getId(), 'eventCourse' => $eventCourses->get($j)->getId()]);
                $formsView[$i][$j] = $modifyParticipantMakeEventCourseForm->createView();
                $modifyParticipantMakeEventCourseForm->handleRequest($request);
                if ($modifyParticipantMakeEventCourseForm->isSubmitted() && $participantMakeEventCourse === $participantMakeEventCourseRepository->find(array("eventCourse" => $this->getDoctrine()->getRepository(EventCourse::class)->find($modifyParticipantMakeEventCourse->getEventCourseId()), "participant" => $this->getDoctrine()->getRepository(Participant::class)->find($modifyParticipantMakeEventCourse->getParticipantId())))) {
                    if (true) {
                        $modifyParticipantMakeEventCourse->modify($this->getDoctrine()->getManager(), $participantMakeEventCourseRepository);
                        return $this->redirect($request->getUri());
                    }
                }
            }
        }

        $manuallySetPenaltyOfParticipantMakeEventCourse = new ManuallySetPenaltyOfParticipantMakeEventCourse();
        $manuallySetPenaltyOfParticipantMakeEventCourseForm = $this->createForm(ManuallySetPenaltyOfParticipantMakeEventCourseType::class, $manuallySetPenaltyOfParticipantMakeEventCourse, ['event' => $event]);
        $manuallySetPenaltyOfParticipantMakeEventCourseForm->handleRequest($request);
        if ($manuallySetPenaltyOfParticipantMakeEventCourseForm->isSubmitted()) {
            if ($manuallySetPenaltyOfParticipantMakeEventCourse->getEventCourse() !== null && $manuallySetPenaltyOfParticipantMakeEventCourse->getParticipant() !== null) {
                $manuallySetPenaltyOfParticipantMakeEventCourse->modify($this->getDoctrine()->getManager(), $participantMakeEventCourseRepository);
                return $this->redirect($request->getUri());
            }
        }


        if (in_array($event, $user->getEvents()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->render(
                'event/my_event_details.html.twig',
                [
                    'event' => $event,
                    'eventCourses' => $event->getEventCourses(),
                    'participants' => $event->getParticipants(),
                    'ranking' => $event->extractRanking($this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class)),
                    'forms' => $formsView,
                    'manuallySetPenaltyFormView' => $manuallySetPenaltyOfParticipantMakeEventCourseForm->createView(),
                ]
            );
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/my_event/details/{id}/csv", name="my_event_csv")
     */
    public function get_my_event_csv($id)
    {
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        if (!$event) {
            return $this->json(["error" => "invalid id"]);
        }
        $fileContent = $event->getCSV($this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));
        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $id . "." . "csv"
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @Route("/my_event/update/{id}", name="my_event_update")
     * @param $id
     * @return RedirectResponse
     */
    public function updateEvent($id): RedirectResponse
    {
        $user = $this->security->getUser();
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        if ($event != null && (in_array($event, $user->getEvents()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles()))) {
            $event->update($this->getDoctrine()->getManager(), $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));

            return $this->redirectToRoute('my_event_details', ['id' => $id]);
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/my_event/delete/{id}", name="my_event_delete")
     * @param $id
     * @return RedirectResponse
     */
    public function deleteEvent($id): RedirectResponse
    {
        $user = $this->security->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        if ($event != null && (in_array($event, $user->getEvents()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles()))) {
            $event->remove($entityManager, $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));

            return $this->redirectToRoute('my_events');
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/my_event/{id}/participants", name="my_event_participants")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function my_event_participants(int $id, Request $request): Response
    {
        $error = "";
        $success = "";
        $participant = new Participant();
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);

        $participantForm = $this->createForm(ParticipantType::class, $participant);
        $participantForm->handleRequest($request);

        if ($participantForm->isSubmitted() && $participantForm->isValid()) {
            $error = $participant->create($event, $this->getDoctrine()->getManager());
            if ($error === "") {
                return $this->redirect($request->getUri());
            }
        }

        return $this->render(
            'event/my_event_manage_participants.html.twig',
            [
                'participantForm' => $participantForm->createView(),
                'participants' => $this->getDoctrine()->getRepository(Event::class)->find($id)->getParticipants(),
                'event' => $this->getDoctrine()->getRepository(Event::class)->find($id),
                'error' => $error,
                'success' => $success,
            ]
        );
    }

    /**
     * @Route("/my_event/{idEvent}/deleteParticipant/{id}", name="my_event_delete_participant")
     * @param int $idEvent
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteParticipant(int $idEvent, int $id): RedirectResponse
    {
        $user = $this->security->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $participant = $this->getDoctrine()->getRepository(Participant::class)->find($id);
        $event = $this->getDoctrine()->getRepository(Event::class)->find($idEvent);
        if ($participant != null && (in_array($event, $user->getEvents()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles()))) {
            $participant->remove($entityManager, $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));
            return $this->redirectToRoute('my_event_participants', ['id' => $idEvent]);
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/my_event/{id}/event_courses", name="my_event_courses")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function my_event_courses(int $id, Request $request): Response
    {
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        $eventCourse = new EventCourse();
        $eventCourseForm = $this->createForm(EventCourseType::class, $eventCourse, ['user' => $this->security->getUser(), 'id' => $eventCourse->getId()]);

        $eventCourseForm->handleRequest($request);

        if ($eventCourseForm->isSubmitted() && $eventCourseForm->isValid() && $eventCourse->getId() === null) {
            $eventCourse->setEvent($event);
            $eventCourse->convertTimeToMaxTime();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eventCourse);

            $eventCourse->generateParticipantMakeEventCourse($entityManager);
            return $this->redirect($request->getUri());
        }
        $modifyFormsView = [];
        foreach ($this->getDoctrine()->getRepository(Event::class)->find($id)->getEventCourses() as $eventCourse) {
            $modifyEventCourse = new ModifyEventCourse($eventCourse);
            $modifyEventCourseForm = $this->createForm(ModifyEventCourseType::class, $modifyEventCourse);
            $modifyFormsView[$eventCourse->getId()] = $modifyEventCourseForm->createView();
            $modifyEventCourseForm->handleRequest($request);
            if ($modifyEventCourseForm->isSubmitted() && $modifyEventCourseForm->isValid() && $modifyEventCourse->getId() === $eventCourse->getId()) {
                $modifyEventCourse->modify($this->getDoctrine()->getRepository(EventCourse::class), $this->getDoctrine()->getManager());
                return $this->redirect($request->getUri());
            }
        }

        return $this->render(
            'event/my_event_courses.html.twig',
            [
                'event' => $event,
                'eventCourseForm' => $eventCourseForm->createView(),
                'eventCourses' => $this->getDoctrine()->getRepository(Event::class)->find($id)->getEventCourses(),
                'idEvent' => $id,
                'modifyEventCourseFormView' => $modifyFormsView,
            ]
        );
    }

    /**
     * @Route("/my_event/{idEvent}/event_courses/removeCourse/{id}", name="my_event_delete_course")
     * @param int $idEvent
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteEventCourse(int $idEvent, int $id): RedirectResponse
    {
        $user = $this->security->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $eventCourse = $this->getDoctrine()->getRepository(EventCourse::class)->find($id);
        $event = $this->getDoctrine()->getRepository(Event::class)->find($idEvent);
        if ($eventCourse != null && (in_array($event, $user->getEvents()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles()))) {
            $eventCourse->remove($entityManager, $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));

            return $this->redirectToRoute('my_event_courses', ['id' => $idEvent]);
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/event/list/", name="events_list")
     */
    public function show_list(): Response
    {
        $allEvents = $this->getDoctrine()->getRepository(Event::class)->findAll();
        return $this->render('event/events_list.html.twig',
            [
                'events' => $allEvents,
            ]
        );
    }

    /**
     * @Route("/event/{id}", name="event_details")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function event_details(int $id, Request $request): Response
    {
        $error = "";
        $success = "";
        $event = $this->getDoctrine()->getRepository(Event::class)->find($id);
        $participant = new Participant();

        $participantForm = $this->createForm(ParticipantType::class, $participant);
        $participantForm->handleRequest($request);

        if ($participantForm->isSubmitted() && $participantForm->isValid()) {
            $participant->create($event, $this->getDoctrine()->getManager());
            return $this->redirect($request->getUri());
        }

        return $this->render('event/event_info.html.twig',
            [
                'event' => $event,
                'eventCourses' => $event->getEventCourses(),
                'participants' => $event->getParticipants(),
                'ranking' => $event->extractRanking($this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class)),
                'participantForm' => $participantForm->createView(),
                'error' => $error,
                'success' => $success,
            ]
        );
    }
}
