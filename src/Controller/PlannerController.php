<?php

namespace App\Controller;

use App\Entity\MissingControlPoint;
use App\Entity\Track;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use App\Entity\Course;
use App\Entity\User;
use App\Model\CreateCourse;
use App\Model\FileUploader;
use function in_array;
use function simplexml_load_string;

class PlannerController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/planner/show_all/{id}", name="your_courses", defaults={"id": -1})
     */
    public function show_all(int $id): Response
    {
        if ($id == -1) {
            $user = $this->security->getUser();
        } else {
            if (in_array('ROLE_ADMIN', $this->security->getUser()->getRoles())) {
                $user = $this->getDoctrine()->getRepository(User::class)->find($id);
            } else {
                throw new AccessDeniedException();
            }
        }

        return $this->render(
            'planner/show_all.html.twig',
            [
                'courses' => $user->getCourses(),
            ]
        );
    }

    /**
     * @Route("/planner/add_course/{id}", name="add_course", defaults={"id": -1})
     * @throws Exception
     */
    public function add_course(int $id, TranslatorInterface $translator, UrlGeneratorInterface $router): Response
    {
        $createCourse = new CreateCourse(
            array(
                "user" => $this->security->getUser(),
                "translator" => $translator,
                "id" => $id,
            ),
            $router,
            $this->getDoctrine()->getRepository(Course::class)
        );

        return $this->render(
            'planner/add_course.html.twig',
            ["data" => $createCourse->initView()]
        );
    }

    /**
     * @Route("/planner/delete-tracks/{id}", name="delete_tracks")
     */
    public function deleteTracks($id, Request $request): Response
    {
        $tracks = $this->getDoctrine()->getRepository(Track::class)->findByCourse($id);
        $formBuilder = $this->createFormBuilder();
        foreach ($tracks as $track) {
            $formBuilder->add($track->getId(), CheckboxType::class, array('label' => $track->getName(), 'required' => false));
        }
        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            foreach ($tracks as $track) {
                if ($data[$track->getId()]) {
                    $em->remove($track);
                }
            }
            $em->flush();

            return $this->redirectToRoute('your_courses');
        }

        return $this->render(
            'planner/delete_tracks.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/planner/missing_control_point/{id}", name="missing_control_point")
     */
    public function missing_control_point($id): Response
    {
        $user = $this->security->getUser();
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (in_array($course, $user->getCourses()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles())) {
            $controlPoints = $course->getMissingControlPoints();

            return $this->render(
                'planner/missing_control_point.html.twig',
                [
                    'missingControlPoints' => $controlPoints,
                ]
            );
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/planner/delete/{id}", name="delete_course")
     */
    public function delete($id): RedirectResponse
    {
        $user = $this->security->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if ($course != null && (in_array($course, $user->getCourses()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles()))) {
            $course->remove($entityManager);

            return $this->redirectToRoute('your_courses');
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/planner/delete_missing_control_point/{id}", name="delete_missing_control_point")
     */
    public function delete_missing_control_point($id): RedirectResponse
    {
        $user = $this->security->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $controlPoint = $this->getDoctrine()->getRepository(MissingControlPoint::class)->find($id);
        $course = $controlPoint->getCourse();
        if (in_array($course, $user->getCourses()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles())) {
            $entityManager->remove($controlPoint);
            $entityManager->flush();

            return $this->redirectToRoute('your_courses');
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/planner/preview/{id}", name="preview_course")
     */
    public function preview($id): Response
    {
        $user = $this->security->getUser();
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (in_array($course, $user->getCourses()->toArray()) || in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->render(
                'planner/preview_course.html.twig',
                [
                    'name' => $course->getName(),
                    'course' => $this->generateUrl('course_info', ['id' => $id]),
                ]
            );
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/planner/course_info/{id}", name="course_info")
     * json data are not well send with render method, have to use xhr to access this function
     */
    function course_info($id): JsonResponse
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);

        $sxml = simplexml_load_string($course->getXml());

        $data1 = array();
        $xmlData = $sxml->RaceCourseData;
        foreach ($xmlData->Course->children() as $children) {
            if ($children->getName() === "CourseControl") {
                $data1[] = [(string)$children->Control, (string)$children["type"]];
            }
        }

        $data2 = array();
        foreach ($sxml->RaceCourseData->children() as $children) {
            if ($children->getName() === "Control") {
                if ($this->control_exists((string)$children->Id, $data1)) {
                    $data2[(string)$children->Id] = array((string)$children->Position["lat"], (string)$children->Position["lng"]);
                }
            }
        }

        return new JsonResponse(
            [
                'xml1' => $data1,
                'xml2' => $data2,
                'kml' => $course->getKml(),
                'image' => $this->generateUrl('image', ['id' => $id]),
            ]
        );
    }

    /**
     * @Route("/planner/upload_file", name="upload_course_file")
     */
    public function upload_file(): JsonResponse
    {
        $request = Request::createFromGlobals();
        $error = "";
        $response = new JsonResponse();
        foreach ($request->files->all() as $file) {
            $user = $this->security->getUser();
            $error = FileUploader::uploadFile($file, $user->getId());
            if (!$error) {
                return $response->fromJsonString($error);
            }
        }

        return $response->fromJsonString($error);
    }

    /**
     * @Route("/planner/upload_course/{id}", name="upload_course", defaults={"id": -1})
     * @throws Exception
     */
    public function upload_course(int $id, TranslatorInterface $translator, UrlGeneratorInterface $router): JsonResponse
    {
        $request = Request::createFromGlobals();
        $createCourse = new CreateCourse(
            array(
                "user" => $this->security->getUser(),
                "translator" => $translator,
                "id" => $id,
            ),
            $router,
            $this->getDoctrine()->getRepository(Course::class)
        );
        if ($request->request->get("preview") == "true") {
            $data = $createCourse->preview($request);
        } else {
            $data = $createCourse->processData($request, $this->getDoctrine()->getManager());
        }

        return new JsonResponse($data);
    }

    private function control_exists(string $param, array $data): bool
    {
        foreach ($data as $datum) {
            if ($datum[0] == $param) {
                return true;
            }
        }

        return false;
    }
}
