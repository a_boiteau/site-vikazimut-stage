<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use DateTime;
use PHPUnit\Util\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Course;
use App\Model\CreateTrack;
use App\Model\CreateMissingControlPoint;
use function dirname;

class DataController extends AbstractController
{
    /**
     * @Route("/data/courses", name="courses")
     */
    public function get_courses(): JsonResponse
    {
        $courses = $this->getDoctrine()->getRepository(Course::class)->findAll();
        foreach ($courses as $course) {
            $courseDate = $course->getStartDate();
            if ($courseDate !== null) {
                $dateDiff = $courseDate->diff(new DateTime('NOW'));
                if ($dateDiff->invert === 1) {
                    unset($courses[array_search($course, $courses)]);
                }
            }
        }
        foreach ($courses as $course) {
            $courseDate = $course->getEndDate();
            if ($courseDate !== null) {
                $dateDiff = $courseDate->diff(new DateTime('NOW'));
                if ($dateDiff->invert === 0) {
                    unset($courses[array_search($course, $courses)]);
                }
            }
        }
        $dataCourses = [];
        foreach ($courses as $course) {
            $info = array(
                "id" => $course->getId(),
                "name" => $course->getName(),
                "latitude" => $course->getLatitude(),
                "longitude" => $course->getLongitude(),
                "length" => $course->getLength(),
            );
            if ($course->getClub() !== null) {
                $info["club"] = $course->getClub();
            }
            array_push($dataCourses, $info);
        }

        return $this->json($dataCourses);
    }

    /**
     * @Route("/data/{id}/xml", name="xml")
     */
    public function get_xml($id)
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            return $this->json(["error" => "invalid id"]);
        }
        $fileContent = $course->getXml();
        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $id."."."xml"
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @Route("/data/{id}/image", name="image")
     */
    public function get_image($id)
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            return $this->json(["error" => "invalid id"]);
        }
        $filename = $course->getImage();
        $response = new Response();

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $acceptedExtensions = array("png", "jpeg", "jpg");
        if (!in_array("BB", $acceptedExtensions)) {
            $ext = "jpeg";
        }
        $response->headers->set('Content-Type', $ext);
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    function encryptFile($encryptKey, $encryptInitializationVector, $inPath): string
    {
        $sourceFile = file_get_contents($inPath);
        $passphrase = base64_decode($encryptKey);
        $initializationVector = base64_decode($encryptInitializationVector);
        $cipherAlgo = 'aes-256-cbc';

        return openssl_encrypt($sourceFile, $cipherAlgo, $passphrase, 0, $initializationVector);
    }

    /**
     * @Route("/data/{id}/img", name="img")
     */
    public function get_image_encrypted_archive($id): Response
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            return $this->json(["error" => "invalid id"]);
        }

        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2).'/config/secret/.image.keys')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        $data = $this->encryptFile($key, $initializationVector, $course->getImage());

        $gzData = gzencode($data);

        $response = new Response($gzData);
        $compressedFilename = $id.".bin".".gz";
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', 'application/octet-stream');
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $compressedFilename);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Length', strlen($gzData));

        return $response;
    }

    /**
     * @Route("/data/{id}/kml", name="kml")
     */
    public function get_kml($id)
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            return $this->json(["error" => "invalid id"]);
        }
        $fileContent = $course->getKml();
        $response = new Response($fileContent);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $id."."."kml"
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @Route("/data/send", name="send_race", methods="POST")
     */
    public function post_race(): JsonResponse
    {
        $request = Request::createFromGlobals();
        $trackCreator = new CreateTrack($request->getContent());
        $isValid = $trackCreator->addTrack(
            $this->getDoctrine()->getManager(),
            $this->getDoctrine()->getRepository(Course::class)
        );
        if ($isValid) {
            try {
                $this->checkAndAddToEvent($trackCreator->getTrack());
            } catch (Exception $e) {
                return new JsonResponse(
                    [
                        'status' => 'error',
                    ],
                    JsonResponse::HTTP_BAD_REQUEST
                );
            }

            return new JsonResponse(
                [
                    'status' => 'ok',
                ],
                JsonResponse::HTTP_OK
            );
        } else {
            return new JsonResponse(
                [
                    'status' => 'error',
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @Route("/data/missing-control-point")
     *
     * To simulate post request:
     * curl -X POST -d \@toto.json https://127.0.0.1:8000/index.php/data/missing-control-point
     */
    public function post_missing_control_point(): JsonResponse
    {
        $request = Request::createFromGlobals();
        $creator = new CreateMissingControlPoint($request->getContent());
        $isValid = $creator->addControlPoint(
            $this->getDoctrine()->getManager(),
            $this->getDoctrine()->getRepository(Course::class)
        );
        if ($isValid) {
            return new JsonResponse(
                [
                    'status' => 'ok',
                ],
                JsonResponse::HTTP_OK
            );
        } else {
            return new JsonResponse(
                [
                    'status' => 'error',
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
    }

    public function checkAndAddToEvent(Track $track)
    {
        $events = $this->getDoctrine()->getRepository(Event::class)->findAll();
        foreach ($events as $event) {
            foreach ($event->getEventCourses() as $eventCourse) {
                if ($eventCourse->getCourse() === $track->getCourse()) {
                    foreach ($event->getParticipants() as $participant) {
                        if ($track->getName() === $participant->getNickname()) {
                            $participantMakeEventCourse = $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant));
                            if ($participantMakeEventCourse->getTrack() === null) {
                                $participantMakeEventCourse->setTrack($track);
                                $entityManager = $this->getDoctrine()->getManager();
                                $entityManager->persist($participantMakeEventCourse);
                                $entityManager->flush();
                                $event->update($entityManager, $this->getDoctrine()->getRepository(ParticipantMakeEventCourse::class));
                            }
                        }
                    }
                }
            }
        }
    }
}
