<?php

namespace App\Controller;

use App\Model\TrackStatistics;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Course;
use App\Entity\Track;
use App\Model\GpxImport;
use App\Form\GpxImportType;
use App\Entity\User;
use function simplexml_load_string;

class CourseController extends AbstractController
{
    const MAX_DISTANCE_IN_METERS = 100_000;

    /**
     * @Route("/routes/{lat}-{lng}", name="course",  requirements={"lat"="{\-?\d+(\.\d+)?}", "lng"="{\-?\d+(\.\d+)?}"}, defaults={"lat": 10000, "lng": 1000})
     */
    public function courses($lat, $lng, TrackStatistics $statistics): Response
    {
        $latitude = (float)substr($lat, 1, strlen($lat) - 2);
        $longitude = (float)substr($lng, 1, strlen($lng) - 2);
        $allCourses = $this->getDoctrine()->getRepository(Course::class)->findAll();
        if ($lat < 10000 && $lng < 1000) { // 1000 and 1000  are the default values...
            $quantifiedCourses = [];
            foreach ($allCourses as $course) {
                $distance = $statistics->distanceInMeters($latitude, $longitude, $course->getLatitude(), $course->getLongitude());
                if ($distance < self::MAX_DISTANCE_IN_METERS) {
                    $quantifiedCourses[] = ["distance" => $distance, "course" => $course];
                }
            }
            usort(
                $quantifiedCourses,
                function ($a, $b) {
                    if ($a["distance"] == $b["distance"]) {
                        return 0;
                    }

                    return $a["distance"] < $b["distance"] ? -1 : 1;
                }
            );
            $courses = [];
            foreach ($quantifiedCourses as $quantifiedCourse) {
                $courses[] = $quantifiedCourse["course"];
            }
        } else {
            $courses = $allCourses;
        }
        foreach ($courses as $course) {
            $course->setImage("data/" . $course->getId() . "/image");
            $courseDate = $course->getStartDate();
            if ($courseDate !== null) {
                $dateDiff = $courseDate->diff(new DateTime('NOW'));
                if ($dateDiff->invert === 1) {
                    unset($courses[array_search($course, $courses)]);
                }
            }
        }

        return $this->render(
            'course/index.html.twig',
            [
                'courses' => $courses,
            ]
        );
    }

    /**
     * @Route("/courses/{id}/tracks", name="show_tracks")
     */
    public function tracks($id, TrackStatistics $statistics, Request $request): Response
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            throw $this->createNotFoundException("Ce parcours n'existe pas : ".$id);
        }
        $courseDate = $course->getStartDate();
        if ($courseDate !== null) {
            $dateDiff = $courseDate->diff(new DateTime('NOW'));
            if ($dateDiff->invert === 1) {
                return $this->render(
                    'course/notPublished.html.twig',
                    [
                        'name' => $course->getName(),
                        'dayLeft' => $dateDiff->format("%a"),
                        'timeLeft' => $dateDiff->format("%hh %im %ss"),
                    ]
                );
            }
        }
        $openImportTab = 0;
        $success = "";
        $xml = $course->getXml();
        $simplexml = simplexml_load_string($xml);
        $lengths = $statistics->computeLegLengths($simplexml);

        $gpxImport = new GpxImport();
        $gpxImport->setIdCourse((int)$id);

        $gpxForm = $this->createForm(GpxImportType::class, $gpxImport);
        $gpxForm->handleRequest($request);
        if ($gpxForm->isSubmitted() && $gpxForm->isValid()) {
            $openImportTab = 1;
            if ($gpxImport->checkGpx()) {
                $courseRepository = $this->getDoctrine()->getRepository(Course::class);
                if ($gpxImport->loadGpx()) {
                    $gpxImport->convertGpxTime();
                    $gpxImport->removeTrackPointsBeforeStart($courseRepository);
                    if ($gpxImport->checkIfIsInBound($courseRepository->find(intval($id))->getKml())) {
                        $gpxImport->addToDataBase(
                            $this->getDoctrine()->getManager(),
                            $courseRepository
                        );
                        $success = "error";
                    }
                }
            }
        }

        return $this->render(
            'course/track.html.twig',
            [
                'name' => $course->getName(),
                'route_map' => $this->generateUrl('course_map', ['id' => $id]),
                'control_points_location' => json_encode(array('distances' => $lengths)),
                'route_track' => $this->generateUrl('course_track', ['id' => $id]),
                'form' => $gpxForm->createView(),
                'error' => $gpxImport->getError(),
                'openImportTab' => $openImportTab,
                'success' => $success,
            ]
        );
    }

    /**
     * @Route("/courses/{id}/info", name="show_info")
     */
    public function courseInfo($id, TrackStatistics $statistics): Response
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if (!$course) {
            throw $this->createNotFoundException('Ce parcours n\'existe pas : '.$id);
        }
        $courseDate = $course->getStartDate();
        if ($courseDate !== null) {
            $dateDiff = $courseDate->diff(new DateTime('NOW'));
            if ($dateDiff->invert === 1) {
                return $this->render(
                    'course/notPublished.html.twig',
                    [
                        'name' => $course->getName(),
                        'dayLeft' => $dateDiff->format("%a"),
                        'timeLeft' => $dateDiff->format("%hh %im %ss"),
                    ]
                );
            }
        }
        $xml = $course->getXml();
        $simplexml = simplexml_load_string($xml);
        $lengths = $statistics->computeLegLengths($simplexml);
        $idCreator = $course->getCreator();
        $user = $this->getDoctrine()->getRepository(User::class)->find($idCreator);
        $date = $course->getUpdateAt();

        return $this->render(
            'course/info.html.twig',
            [
                'name' => $course->getName(),
                'club' => $course->getClub(),
                'route_map' => $this->generateUrl('course_map', ['id' => $id]),
                'creator' => $user->getUsername(),
                'day' => $date->format("d"),
                'month' => $date->format("m"),
                'year' => $date->format("Y"),
                'control_point_number' => count($lengths),
                'length' => $course->getLength(),
                'isPrintable' => $course->getPrintable(),
            ]
        );
    }

    /**
     * @Route("/courses/course_info/{id}", name="course_map")
     * json data are not well send with render method, have to use xhr to access this function
     */
    function courseMap($id): JsonResponse
    {
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        if ($course == null) {
            throw $this->createNotFoundException('Ce parcours n\'existe pas : '.$id);
        }

        return new JsonResponse(
            [
                'kml' => $course->getKml(),
                'image' => substr($this->generateUrl('image', ['id' => $id]),1),

            ]
        );
    }

    /**
     * @Route("/courses/control_point/{id}", name="control_point")
     */
    function controlPoints($id): JsonResponse
    {
        $track = $this->getDoctrine()->getRepository(Track::class)->find($id);

        return new JsonResponse(
            [
                'controlPoints' => $track->getControlPoints(),
            ]
        );
    }

    /**
     * @Route("/courses/course_track/{id}", name="course_track")
     * json data are not well send with render method, have to use xhr to access this function
     */
    function courseTrack($id, TrackStatistics $statistics): JsonResponse
    {
        $tracks = $this->getDoctrine()->getRepository(Track::class)->findByCourse($id);
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);
        $xml = $course->getXml();
        $simplexml = simplexml_load_string($xml);
        $lengths = $statistics->computeLegLengths($simplexml);
        $routes = array();
        foreach ($tracks as $track) {
            $splitTimes = $statistics->computeSplitTime($track->getControlPoints());
            $distances = $statistics->computeActualSplitDistance($track->getControlPoints(), $track->getTrace());
            $rk = $statistics->computeSplitPaces($splitTimes, $lengths);
            array_push(
                $routes,
                [
                    $track->getName(),
                    $track->getTotalTime(),
                    $track->getTrace(),
                    $track->getControlPoints(),
                    $track->getFormat(),
                    $splitTimes,
                    $distances,
                    $rk,
                    $track->getImported(),
                ]
            );
        }

        return new JsonResponse(['tracks' => $routes]);
    }

    /**
     * @Route("/worldmap",name="worldmap")
     */
    public function worldMap(): Response
    {
        $courses = $this->getDoctrine()->getRepository(Course::class)->findAll();
        foreach ($courses as $course) {
            $courseDate = $course->getStartDate();
            if ($courseDate !== null) {
                $dateDiff = $courseDate->diff(new DateTime('NOW'));
                if ($dateDiff->invert === 1) {
                    unset($courses[array_search($course, $courses)]);
                }
            }
        }

        return $this->render(
            'course/worldmap.html.twig',
            [
                'courses' => $courses,
                'total_courses' => count($courses),
            ]
        );
    }
}
