<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210421092312 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE missing_control_point CHANGE control_point_id control_point_id VARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE track ADD imported TINYINT(1) NOT NULL DEFAULT false, CHANGE format format INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE email email LONGTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE missing_control_point CHANGE control_point_id control_point_id INT NOT NULL');
        $this->addSql('ALTER TABLE track DROP imported, CHANGE format format INT DEFAULT 0');
        $this->addSql('ALTER TABLE user CHANGE email email TEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
