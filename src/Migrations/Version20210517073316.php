<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517073316 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, name VARCHAR(255) NOT NULL, type INT NOT NULL, INDEX IDX_3BAE0AA761220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_course (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, course_id INT NOT NULL, format INT NOT NULL, missing_punch_penalty INT NOT NULL, over_time_penalty INT NOT NULL, max_time INT DEFAULT NULL, INDEX IDX_240718EB71F7E88B (event_id), INDEX IDX_240718EB591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, nickname VARCHAR(255) NOT NULL, INDEX IDX_D79F6B1171F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant_make_event_course (event_course_id INT NOT NULL, participant_id INT NOT NULL, track_id INT DEFAULT NULL, score INT NOT NULL, nb_over_time_penalty INT NOT NULL, nb_missing_punch_penalty INT NOT NULL, modified TINYINT(1) NOT NULL, INDEX IDX_7E1597E0DF89F7AF (event_course_id), INDEX IDX_7E1597E09D1C3019 (participant_id), INDEX IDX_7E1597E05ED23C43 (track_id), PRIMARY KEY(event_course_id, participant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA761220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE event_course ADD CONSTRAINT FK_240718EB71F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE event_course ADD CONSTRAINT FK_240718EB591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B1171F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE participant_make_event_course ADD CONSTRAINT FK_7E1597E0DF89F7AF FOREIGN KEY (event_course_id) REFERENCES event_course (id)');
        $this->addSql('ALTER TABLE participant_make_event_course ADD CONSTRAINT FK_7E1597E09D1C3019 FOREIGN KEY (participant_id) REFERENCES participant (id)');
        $this->addSql('ALTER TABLE participant_make_event_course ADD CONSTRAINT FK_7E1597E05ED23C43 FOREIGN KEY (track_id) REFERENCES track (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_course DROP FOREIGN KEY FK_240718EB71F7E88B');
        $this->addSql('ALTER TABLE participant DROP FOREIGN KEY FK_D79F6B1171F7E88B');
        $this->addSql('ALTER TABLE participant_make_event_course DROP FOREIGN KEY FK_7E1597E0DF89F7AF');
        $this->addSql('ALTER TABLE participant_make_event_course DROP FOREIGN KEY FK_7E1597E09D1C3019');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE event_course');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE participant_make_event_course');
    }
}
