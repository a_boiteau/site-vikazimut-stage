<?php

namespace App\DataFixtures;

use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParticipantFixtures extends Fixture implements DependentFixtureInterface
{
    public const SULIAC_Championship_Participant_REFERENCE = 'Suliac_championship_participant';
    public const ERIC_Championship_Participant_REFERENCE = 'Eric_championship_participant';
    public const SULIAC_Points_Participant_REFERENCE = 'Suliac_points_participant';
    public const ERIC_Points_Participant_REFERENCE = 'Eric_points_participant';
    public const SULIAC_Cumulated_Time_Participant_REFERENCE = 'Suliac_cumulated_time_participant';
    public const ERIC_Cumulated_Time_Participant_REFERENCE = 'Eric_cumulated_time_participant';

    public function load(ObjectManager $manager)
    {
        $participant = new Participant();
        $participant->setNickname("Suliac");
        $participant->setEvent($this->getReference(EventFixtures::Championship_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::SULIAC_Championship_Participant_REFERENCE, $participant);

        $participant = new Participant();
        $participant->setNickname("Eric");
        $participant->setEvent($this->getReference(EventFixtures::Championship_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::ERIC_Championship_Participant_REFERENCE, $participant);


        $participant = new Participant();
        $participant->setNickname("Suliac");
        $participant->setEvent($this->getReference(EventFixtures::Points_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::SULIAC_Points_Participant_REFERENCE, $participant);

        $participant = new Participant();
        $participant->setNickname("Eric");
        $participant->setEvent($this->getReference(EventFixtures::Points_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::ERIC_Points_Participant_REFERENCE, $participant);


        $participant = new Participant();
        $participant->setNickname("Suliac");
        $participant->setEvent($this->getReference(EventFixtures::Cumulated_Time_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::SULIAC_Cumulated_Time_Participant_REFERENCE, $participant);

        $participant = new Participant();
        $participant->setNickname("Eric");
        $participant->setEvent($this->getReference(EventFixtures::Cumulated_Time_EVENT_REFERENCE));
        $manager->persist($participant);
        $this->addReference(self::ERIC_Cumulated_Time_Participant_REFERENCE, $participant);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EventFixtures::class
        ];
    }
}
