<?php

namespace App\DataFixtures;

use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    public const Championship_EVENT_REFERENCE = 'event_championship';
    public const Points_EVENT_REFERENCE = 'event_points';
    public const Cumulated_Time_EVENT_REFERENCE = 'event_cumulated_time';

    public function load(ObjectManager $manager)
    {
        $event = new Event();
        $event->setName("Championship");
        $event->setCreator($this->getReference(UserFixtures::TEST_USER_REFERENCE));
        $event->setType(0);
        $manager->persist($event);
        $this->addReference(self::Championship_EVENT_REFERENCE, $event);

        $event = new Event();
        $event->setName("Points");
        $event->setCreator($this->getReference(UserFixtures::TEST_USER_REFERENCE));
        $event->setType(1);
        $manager->persist($event);
        $this->addReference(self::Points_EVENT_REFERENCE, $event);

        $event = new Event();
        $event->setName("Cumulated_Time");
        $event->setCreator($this->getReference(UserFixtures::TEST_USER_REFERENCE));
        $event->setType(2);
        $manager->persist($event);
        $this->addReference(self::Cumulated_Time_EVENT_REFERENCE, $event);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
