<?php

namespace App\DataFixtures;

use App\Entity\ParticipantMakeEventCourse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ParticipantMakeEventCourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_CHAMPIONSHIP_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Championship_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_CHAMPIONSHIP_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Championship_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::ERIC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_CHAMPIONSHIP_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Championship_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_AUBIN_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_CHAMPIONSHIP_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Championship_Participant_REFERENCE));
        $manager->persist($participantMakeEventCourse);
        /////////////////////////////////

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_POINTS_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Points_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_POINTS_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Points_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::ERIC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_POINTS_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Points_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_AUBIN_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_POINTS_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Points_Participant_REFERENCE));
        $manager->persist($participantMakeEventCourse);
        /////////////////////////////////

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_CUMULATED_TIME_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Cumulated_Time_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::LUC_CUMULATED_TIME_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Cumulated_Time_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::ERIC_LUC_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_CUMULATED_TIME_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::SULIAC_Cumulated_Time_Participant_REFERENCE));
        $participantMakeEventCourse->setTrack($this->getReference(TrackFixtures::SULIAC_AUBIN_TRACK_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $participantMakeEventCourse = new ParticipantMakeEventCourse();
        $participantMakeEventCourse->setEventCourse($this->getReference(EventCourseFixtures::AUBIN_CUMULATED_TIME_EVENTCOURSE_REFERENCE));
        $participantMakeEventCourse->setParticipant($this->getReference(ParticipantFixtures::ERIC_Cumulated_Time_Participant_REFERENCE));
        $manager->persist($participantMakeEventCourse);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EventCourseFixtures::class,
            ParticipantFixtures::class
        ];
    }
}
