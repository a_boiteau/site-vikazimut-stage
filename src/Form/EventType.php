<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => "form.event.name",])
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => "form.event.type",
                    'choices' => [
                        'form.event.championship' => 0,
                        'form.event.points' => 1,
                        'form.event.cumulativeTime' => 2,
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'event' => Event::class,
            ]
        );
    }
}