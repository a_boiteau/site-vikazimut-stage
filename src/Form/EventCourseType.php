<?php

namespace App\Form;

use App\Entity\EventCourse;
use App\Entity\User;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $courses = [];
        foreach ($options['user']->getCourses() as $course) {
            $courses[$course->getName()] = $course;
        }
        $builder
            ->add('id', HiddenType::class, ['data' => $options['id']])
            ->add(
                'course',
                ChoiceType::class,
                [
                    'label' => "form.eventCourse.course",
                    'choices' => $courses,
                ]
            )
            ->add(
                'format',
                ChoiceType::class,
                [
                    'label' => "form.eventCourse.format",
                    'choices' => [
                        'form.eventCourse.imposed' => 0,
                        'form.eventCourse.free' => 1,
                    ],
                ]
            )
            ->add('missingPunchPenalty', TextType::class, ['label' => "form.eventCourse.missingPunchPenalty",])
            ->add('overTimePenalty', TextType::class, ['label' => "form.eventCourse.overTimePenalty",])
            ->add(
                'time',
                TimeType::class,
                [
                    'input' => 'timestamp',
                    'widget' => 'choice',
                    'label' => "form.eventCourse.maxTime",
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'eventCourse' => EventCourse::class,
                'user' => User::class,
                'id' => Integer::class,
            ]
        );
    }
}