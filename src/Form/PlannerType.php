<?php

namespace App\Form;

use App\Entity\User;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class PlannerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, ['label' => 'user.identifier']);
        if ($options["password"]) {
            $builder->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les deux mots de passe sont différents.',
                    'required' => false,
                    'first_options' => ['label' => 'user_modify.new_password'],
                    'second_options' => ['label' => 'user_modify.renew_password'],
                ]
            );
        }
        $builder->add('email', TextType::class, ['label' => 'email']);
        $builder->add('phone', TextType::class, ['label' => 'phone', 'required' => false,]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'password' => Boolean::class,
            ]
        );
    }
}