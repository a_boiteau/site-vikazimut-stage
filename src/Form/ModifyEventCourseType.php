<?php

namespace App\Form;

use App\Model\ModifyEventCourse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModifyEventCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class)
            ->add(
                'format',
                ChoiceType::class,
                [
                    'label' => "form.eventCourse.format",
                    'choices' => [
                        'form.eventCourse.imposed' => 0,
                        'form.eventCourse.free' => 1,
                    ],
                ]
            )
            ->add('missingPunchPenalty', TextType::class, ['label' => "form.eventCourse.missingPunchPenalty",])
            ->add('overTimePenalty', TextType::class, ['label' => "form.eventCourse.overTimePenalty",])
            ->add(
                'time',
                TimeType::class,
                [
                    'input' => 'timestamp',
                    'widget' => 'choice',
                    'label' => "form.eventCourse.maxTime",
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'modifyEventCourse' => ModifyEventCourse::class,
            ]
        );
    }
}