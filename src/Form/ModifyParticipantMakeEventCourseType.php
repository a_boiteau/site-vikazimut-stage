<?php

namespace App\Form;

use App\Entity\Course;
use App\Model\ModifyParticipantMakeEventCourse;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModifyParticipantMakeEventCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tracks = [];
        $tracksSimple = [];
        $duplicates = [];
        $tracks["-"] = null;
        foreach ($options['course']->getOrienteer() as $track) {
            if (array_key_exists($track->getName(), $duplicates)) {
                if ($duplicates[$track->getName()] === 1) {
                    $timeOfTheTrack = $tracksSimple[$track->getName()]->getTotalTimeInHMS();
                    $tracks[$track->getName()." [1] (".$timeOfTheTrack.")"] = $tracks[$track->getName()." (".$timeOfTheTrack.")"];
                    unset($tracks[$track->getName()." (".$timeOfTheTrack.")"]);
                }
                $duplicates[$track->getName()] += 1;
                $tracks[$track->getName()." [".$duplicates[$track->getName()]."] (".$track->getTotalTimeInHMS().")"] = $track;

            } else {
                $tracks[$track->getName()." (".$track->getTotalTimeInHMS().")"] = $track;
                $tracksSimple[$track->getName()] = $track;
                $duplicates[$track->getName()] = 1;
            }
        }
        $builder->add(
            'track',
            ChoiceType::class,
            [
                'label' => "form.modifyParticipantMakeEventCourse.track",
                'choices' => $tracks,
            ]
        )
            ->add('participantId', HiddenType::class, ['data' => $options['participant']])
            ->add('eventCourseId', HiddenType::class, ['data' => $options['eventCourse']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'modifyParticipantMakeEventCourse' => ModifyParticipantMakeEventCourse::class,
                'course' => Course::class,
                'participant' => Integer::class,
                'eventCourse' => Integer::class,
            ]
        );
    }
}