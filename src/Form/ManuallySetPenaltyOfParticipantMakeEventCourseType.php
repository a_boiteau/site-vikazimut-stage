<?php

namespace App\Form;

use App\Entity\Event;
use App\Model\ManuallySetPenaltyOfParticipantMakeEventCourse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ManuallySetPenaltyOfParticipantMakeEventCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $event = $options['event'];
        $participants = [];
        $participants["-"] = null;
        foreach ($event->getParticipants() as $participant) {
            $participants[$participant->getNickName()] = $participant;
        }
        $eventCourses = [];
        $eventCourses["-"] = null;
        foreach ($event->getEventCourses() as $eventCourse) {
            $eventCourses[$eventCourse->getCourse()->getName()] = $eventCourse;
        }
        $builder
            ->add(
                'participant',
                ChoiceType::class,
                [
                    'label' => "form.ManuallySetPenalty.participant",
                    'choices' => $participants,
                ]
            )
            ->add(
                'eventCourse',
                ChoiceType::class,
                [
                    'label' => "form.ManuallySetPenalty.eventCourse",
                    'choices' => $eventCourses,
                ]
            )
            ->add('nbPM', TextType::class, ['required' => false, 'label' => "form.ManuallySetPenalty.nbPM",])
            ->add('nbOT', TextType::class, ['required' => false, 'label' => "form.ManuallySetPenalty.nbOT",]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'manuallySetPenaltyOfParticipantMakeEventCourse' => ManuallySetPenaltyOfParticipantMakeEventCourse::class,
                'event' => Event::class,
            ]
        );
    }
}