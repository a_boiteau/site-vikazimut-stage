<?php

namespace App\Form;

use App\Model\GpxImport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GpxImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickname', TextType::class, ['label' => "import.gpx.nickname",])
            ->add('gpx', FileType::class, ['label' => "import.gpx.file",])
            ->add(
                'format',
                ChoiceType::class,
                [
                    'label' => "import.gpx.route.type",
                    'choices' => [
                        'import.gpx.route.preset' => 0,
                        'import.gpx.route.free' => 1,
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'GpxImport' => GpxImport::class,
            ]
        );
    }
}