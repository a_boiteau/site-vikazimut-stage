<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer {
    public function configure()
    {
        return $this->getConfigBuilder()
            ->server('u51382475@home468677337.1and1-data.host')
            ->deployDir('/appli/')
            ->repositoryUrl('git@gitlab.ecole.ensicaen.fr:rclouard/vikazimut-site.git')
            ->repositoryBranch('master');
    }

    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    public function beforeFinishingDeploy()
    {
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
    }
};
