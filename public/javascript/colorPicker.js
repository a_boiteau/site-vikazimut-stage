class ColorPicker {

    constructor() {
        this._basic_color = [
            "#0000FF",
            "#FF1493",
            "#8B008B",
            "#B22222",
            "#FA8072",
            "#8A2BE2",
            "#DEB887",
            "#BFBC8F",
            "#5F9EA0",
            "#FF7F50",
            "#6495ED",
            "#B8860B",
            "#556B2F",
            "#9932CC",
            "#2F4F4F"
        ];
    }

    removeColor() {
        let color;
        if (this._basic_color.length > 0) {
            color = this._basic_color[0];
            this._basic_color.splice(0, 1);
        } else {
            color = this._getRandomColor();
        }
        return color;
    }

    addColor(color) {
        this._basic_color.unshift(color);
    }

    _getRandomColor() {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}