//use with leaflet.js

let controlPointsMarkers;
let recenterButton;

function deg2rad(degree) {
    return (degree * Math.PI) / 180;
}

//provided by https://stackoverflow.com/a/1624732
function calculateCorner(kml) {
    let n = parseFloat(kml.getElementsByTagName("north")[0].childNodes[0].nodeValue);
    let s = parseFloat(kml.getElementsByTagName("south")[0].childNodes[0].nodeValue);
    let e = parseFloat(kml.getElementsByTagName("east")[0].childNodes[0].nodeValue);
    let w = parseFloat(kml.getElementsByTagName("west")[0].childNodes[0].nodeValue);
    let rot = parseFloat(kml.getElementsByTagName("rotation")[0].childNodes[0].nodeValue);
    let angle = deg2rad(rot);

    let center_latitude = (n + s) / 2;
    let center_longitude = (w + e) / 2;
    let squish_latitude = Math.cos(deg2rad(center_latitude));
    let offset_longitude = squish_latitude * (e - w) / 2;
    let offset_latitude = (n - s) / 2;

    let nw = [center_latitude - offset_longitude * Math.sin(angle) + offset_latitude * Math.cos(angle),
        center_longitude - (offset_longitude * Math.cos(angle) + offset_latitude * Math.sin(angle)) / squish_latitude
    ];

    let ne = [center_latitude + offset_longitude * Math.sin(angle) + offset_latitude * Math.cos(angle),
        center_longitude + (offset_longitude * Math.cos(angle) - offset_latitude * Math.sin(angle)) / squish_latitude
    ];

    let se = [center_latitude + offset_longitude * Math.sin(angle) - offset_latitude * Math.cos(angle),
        center_longitude + (offset_longitude * Math.cos(angle) + offset_latitude * Math.sin(angle)) / squish_latitude];

    let sw = [center_latitude - offset_longitude * Math.sin(angle) - offset_latitude * Math.cos(angle),
        center_longitude - (offset_longitude * Math.cos(angle) - offset_latitude * Math.sin(angle)) / squish_latitude];

    return [nw, ne, se, sw];
}

function generateMap(map_div) {
    let map = new L.Map(map_div);
    let osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'});
    map.addLayer(osm);
    L.control.scale({metric: true, imperial: false}).addTo(map);
    return map;
}

function previewXml(data, divXml) {
    while (divXml.lastElementChild) {
        divXml.removeChild(divXml.lastElementChild);
    }

    if (!(typeof data.xml1 == "object")) {
        let error = document.createElement("p");
        error.setAttribute("class", "alert alert-danger");
        error.innerHTML = data.xml1;
        divXml.appendChild(error);
    } else {
        let tableSection = document.createElement("table");
        tableSection.setAttribute("class", "table");
        let tableHead = document.createElement("thead");
        let rowHead = document.createElement("tr");
        let indexColumnHead = document.createElement("th");
        indexColumnHead.setAttribute("scope", "col");
        indexColumnHead.innerHTML = translations.number;
        let nameColumnHead = document.createElement("th");
        nameColumnHead.setAttribute("scope", "col");
        nameColumnHead.innerHTML = translations.label;
        let typeColumnHead = document.createElement("th");
        typeColumnHead.setAttribute("scope", "col");
        typeColumnHead.innerHTML = translations.type;

        rowHead.appendChild(indexColumnHead);
        rowHead.appendChild(nameColumnHead);
        rowHead.appendChild(typeColumnHead);
        tableHead.appendChild(rowHead);
        tableSection.appendChild(tableHead);
        let tableBody = document.createElement("tbody");
        tableSection.appendChild(tableBody);
        const total = data.xml1.length;
        for (let [key, value] of Object.entries(data.xml1)) {
            let row = document.createElement("tr");
            let indexCell = document.createElement("td");
            indexCell.setAttribute("scope", "row");
            const index = parseInt(key, 10);
            if (index === 0) {
                indexCell.innerHTML = "Dép."
            } else if (index === total - 1) {
                indexCell.innerHTML = "Arr."
            } else {
                indexCell.innerHTML = index;
            }
            let nameCell = document.createElement("td");
            nameCell.innerHTML = value[0];
            let typeCell = document.createElement("td");
            typeCell.innerHTML = value[1];
            row.appendChild(indexCell);
            row.appendChild(nameCell);
            row.appendChild(typeCell);
            tableSection.appendChild(row);
        }
        divXml.appendChild(tableSection);
    }
}

function previewMap(data, divMap, divLeaflet, map, overlay, img) {
    fetch(window.location.origin + "/" + data.image).then(function (response) {
        if (response.status === 200) {
            overlay.setUrl(response.url);
            img.onload = function () {
                let quoWidthHeight = this.naturalHeight / this.naturalWidth;
                img.width = divMap.clientWidth / 2;
                img.height = divMap.clientWidth / 2 * quoWidthHeight;
            }
            img.src = response.url;
        } else {
            let error = document.getElementById("map-not-found");
            error.style.display = "block";
            img.width = 0;
            img.height = 0;
            overlay.setUrl("/");
            img.src = "";
        }
    });

    try {
        let lines = data.kml.split('\n');
        lines.splice(0, 1);
        let kml = lines.join('\n');
        let parser = new DOMParser();
        let kmlDoc = parser.parseFromString(kml, "text/xml");
        divLeaflet.style.height = Math.floor(window.innerHeight * 0.75) + "px";
        divMap.appendChild(divLeaflet);
        let corner = calculateCorner(kmlDoc);
        overlay.reposition(L.latLng(corner[0]), L.latLng(corner[1]), L.latLng(corner[3]));
        map.fitBounds([corner[0], corner[2]]);
        // Add recenter button
        if (recenterButton != null) {
            recenterButton.remove();
        }
        recenterButton = L.easyButton('fa-map-marker', function (btn, map) {
            map.fitBounds([corner[0], corner[2]]);
        }, 'Zoom To Home');
        recenterButton.addTo(map);
    } catch (e) {
        let error = document.createElement("p");
        error.setAttribute("class", "alert alert-danger");
        error.innerHTML = data.kml;
        divLeaflet.style.width = "0px";
        divLeaflet.style.height = "0px";
        divMap.appendChild(error);
        divMap.appendChild(img);
    }
}

function addControlPointOnMap(data, map) {
    controlPointsMarkers.clearLayers();
    let usedControlPoints = [];
    let markerColor;
    let markerOpacity;
    for (let [key, value] of Object.entries(data.xml1)) {
        usedControlPoints.push(value[0]);
    }
    for (let [key, value] of Object.entries(data.xml2)) {
        if (usedControlPoints.includes(key)) {
            markerColor = '#ff0000';
            markerOpacity = 0.8;
        } else {
            markerColor = '#1f479f';
            markerOpacity = 0.5;
        }
        const a = L.circleMarker(value, {
            radius: 8.0,
            fillColor: markerColor,
            color: '#000000',
            weight: 1,
            opacity: markerOpacity,
            fillOpacity: markerOpacity
        }).addTo(controlPointsMarkers);
        a.bindPopup(key, {maxWidth: "none"})
    }
}

function showPreview(data, divXml, divMap, divLeaflet, map, overlay, img) {
    if (controlPointsMarkers == null) {
        controlPointsMarkers = L.layerGroup();
        controlPointsMarkers.addTo(map);
    }
    previewXml(data, divXml);
    previewMap(data, divMap, divLeaflet, map, overlay, img);
    addControlPointOnMap(data, map);
}

function downloadMap(imageURL) { //see library printJS for further print options.
    let printableMap = window.open(window.location.origin + "/" + imageURL);
    printableMap.download();
}

function switchMapDisplay(overlay, overlayOpacity) {
    let hideButton = document.getElementById("hide-map");
    let showButton = document.getElementById("show-map");
    if (overlayOpacity === 100) {
        overlayOpacity = 0;
        hideButton.style.display = "block";
        showButton.style.display = "none";
    } else {
        overlayOpacity = 100;
        hideButton.style.display = "none";
        showButton.style.display = "block";
    }
    overlay.setOpacity(overlayOpacity);
}