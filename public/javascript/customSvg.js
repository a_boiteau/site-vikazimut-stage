class CustomeSvg {
    static valid = "M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z";
    static valid_color = "green";
    static invalid = "M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z";
    static invalid_color = "red";
    static modify = "M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 011.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z";
    static modify_color = "orange";

    constructor(type) {
        this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");

        this.svg.setAttribute("viewBox", "0 0 12 16");
        this.svg.setAttribute("width", "12");
        this.svg.setAttribute("height", "16");
        this.svg.setAttribute("style", "fill: currentColor;");

        this.changeSvg(type);
    }

    changeSvg(type) {
        let path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        path.setAttribute("d", type);
        path.setAttribute("fill-rule", "evenodd");

        while (this.svg.lastElementChild) {
            this.svg.removeChild(this.svg.lastElementChild);
        }

        this.svg.appendChild(path);
    }

    remove(modify, span) {
        if (modify) {
            span.setAttribute("style", "color:" + CustomeSvg.modify_color + ";");
            this.changeSvg(CustomeSvg.modify);
        } else {
            span.setAttribute("style", "color:" + CustomeSvg.invalid_color + ";");
            this.changeSvg(CustomeSvg.invalid);
        }
    }

    checkStatus() {
        return this.svg.getElementsByTagName("path")[0].getAttribute("d") === CustomeSvg.valid;
    }
}