function openTab(tabTitle, tabId, tabContentClass) {
    let i, tabContent, tabLinks;
    let activatedTab = document.getElementById(tabTitle);
    tabContent = document.getElementsByClassName(tabContentClass);
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("tab-links");
    for (i = 0; i < tabLinks.length; i++) {
        if (activatedTab.parentNode == tabLinks[i].parentNode){
            tabLinks[i].className = tabLinks[i].className.replace(" active", "");
        }
    }
    document.getElementById(tabId).style.display = "block";
    activatedTab.className += " active";
}

