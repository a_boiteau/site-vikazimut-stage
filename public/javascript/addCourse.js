function initIcon(modify, span, icon) {
    if (modify) {
        icon = new CustomeSvg(CustomeSvg.modify);
        span.setAttribute("style", "color:" + CustomeSvg.modify_color + ";");
    } else {
        icon = new CustomeSvg(CustomeSvg.invalid);
        span.setAttribute("style", "color:" + CustomeSvg.invalid_color + ";");
    }
    span.appendChild(icon.svg);

    return icon;
}
