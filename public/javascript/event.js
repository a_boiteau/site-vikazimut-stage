window.addEventListener("load", function () {
    msTimes = document.getElementsByClassName("msTime");
    for (let i = 0; i < msTimes.length; i++) {
        convertTime(msTimes[i]);
    }
});

function convertTime(event) {
    if (!event.innerHTML.includes("-")) {
        let ms = event.innerHTML;
        let sec = ms / 1000;
        let min = 0;
        let hou = 0;
        let str = "";
        if (sec >= 3600) {
            hou = Math.floor(sec / 3600);
            sec = sec % 3600;
        }
        if (sec >= 60) {
            min = Math.floor(sec / 60);
            sec = sec % 60;
        }
        if (hou.toString().length == 1) {
            str = "0" + hou.toString() + "h";
        } else {
            str = hou.toString() + "h";
        }
        if (min.toString().length == 1) {
            str += "0" + min.toString() + "m";
        } else {
            str += min.toString() + "m";
        }
        sec = Math.floor(sec);
        if (sec.toString().length == 1) {
            str += "0" + sec.toString() + "s";
        } else {
            str += sec.toString() + "s";
        }
        event.innerHTML = str;
    }
}