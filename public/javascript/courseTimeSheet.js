function addTimeSheet(pseudo, punchedPoints, splitTimes, distance, pace) {
    timeSheetData.push([pseudo, punchedPoints, splitTimes, distance, pace]);
    showTimeSheet();
}

function removeTimeSheet(event, pseudo) {
    timeSheetData.forEach(function (orienteer, index) {
        if (orienteer[0] === pseudo) {
            timeSheetData.splice(index, 1);
        }
    });
    showTimeSheet();
}

function clearTimeSheet() {
    timeSheetData = [];
    showTimeSheet();
}

function convertTimeToString(time) {
    let timeInS = time / 1000.0;
    let hrs = Math.floor(timeInS / 3600);
    timeInS = timeInS - hrs * 3600;

    let min = Math.floor(timeInS / 60);
    timeInS = timeInS - min * 60;
    let minutes = min < 10 ? "0" + min : "" + min;

    let sec = Math.floor(timeInS);
    let seconds = sec < 10 ? "0" + sec : "" + sec;

    if (hrs > 0) {
        return hrs + ":" + minutes + ":" + seconds;
    } else {
        return minutes + ":" + seconds;
    }
}

function deltaDistanceCalculator(idealDistance, realDistance) {
    let distanceString = idealDistance.toString();
    (idealDistance > +realDistance) ? distanceString += " (+" : distanceString += " (";
    distanceString += Math.round(((idealDistance / realDistance) - 1) * 100) + "%)";
    return distanceString;
}

function computePace(timeInMilliseconds, distanceInMeter) {
    return ((timeInMilliseconds) / (60 * distanceInMeter)).toFixed(1);
}

function computeSpeed(timeInMilliseconds, distanceInMeter) {
    return (3600 * distanceInMeter / timeInMilliseconds).toFixed(1);
}

function renderTimeSheet(distances) {
    if (timeSheetData.length === 0) {
        return document.createElement("p");
    }
    let tableElement = document.createElement("table");
    tableElement.setAttribute("class", "table");

    // Head
    let headRow1 = document.createElement("tr");
    let cell = document.createElement("th");
    cell.setAttribute("scope", "col");
    headRow1.appendChild(cell);
    cell = document.createElement("th");
    cell.setAttribute("scope", "col");
    headRow1.appendChild(cell);

    let headRow2 = document.createElement("tr");
    cell = document.createElement("th");
    cell.setAttribute("scope", "col");
    cell.className = "align-middle";
    cell.innerHTML = translations.controlPoint;
    headRow2.appendChild(cell);
    cell = document.createElement("th");
    cell.setAttribute("scope", "col");
    cell.className = "align-middle";
    cell.innerHTML = translations.length + " (m)";
    headRow2.appendChild(cell);

    let head = document.createElement("thead");
    head.appendChild(headRow1);
    head.appendChild(headRow2);

    // Body
    let body = document.createElement("tbody");
    let rows = [];
    let cps = timeSheetData[0][1];
    let totalTheoreticalDistance = 0;
    for (let i = 0; i < cps.length; i++) {
        const index = cps[i]["controlPoint"];
        let controlCell = document.createElement("th");
        let lengthCell = document.createElement("td");
        controlCell.setAttribute("scope", "col");
        if (index === 0) {
            controlCell.innerHTML = translations.start;
        } else if (index === timeSheetData[0][1].length - 1) {
            controlCell.innerHTML = translations.finish;
            lengthCell.innerHTML = distances[i];
            totalTheoreticalDistance += distances[i];
        } else {
            controlCell.innerHTML = index;
            lengthCell.innerHTML = distances[i];
            totalTheoreticalDistance += distances[i];
        }
        let row = document.createElement("tr");
        row.appendChild(controlCell);
        row.appendChild(lengthCell);
        body.appendChild(row);
        rows.push(row);
    }
    let totalTitleCell = document.createElement("th");
    totalTitleCell.innerHTML = translations.total;
    let totalLengthCell = document.createElement("th");
    totalLengthCell.innerHTML = totalTheoreticalDistance.toString();
    let totalsRow = document.createElement("tr");
    totalsRow.appendChild(totalTitleCell);
    totalsRow.appendChild(totalLengthCell);
    body.appendChild(totalsRow);

    // Body tracks
    timeSheetData.forEach(function (orienteerData) {
        let cell = document.createElement("th");
        cell.colSpan = 3;
        cell.setAttribute("scope", "col");
        cell.innerHTML = orienteerData[0];
        headRow1.appendChild(cell);

        cell = document.createElement("th");
        cell.setAttribute("scope", "col");
        cell.className = "align-middle";
        cell.innerHTML = translations.duration;
        headRow2.appendChild(cell);
        cell = document.createElement("th");
        cell.setAttribute("scope", "col");
        cell.className = "align-middle";
        cell.innerHTML = translations.distance + " (m)";
        headRow2.appendChild(cell);
        cell = document.createElement("th");
        cell.setAttribute("scope", "col");
        cell.className = "align-middle";
        cell.innerHTML = translations.pace + " (min/km) / <br>" + translations.speed + " (km/h)";
        headRow2.appendChild(cell);

        let orienteerTotalDistance = 0;
        let orienteerTotalTime = 0;

        for (let i = 0; i < orienteerData[1].length; i++) {
            let index = i;
            let timeCell = document.createElement("td");
            timeCell.style.background = "lightgray";
            timeCell.setAttribute("scope", "col");
            let distanceCell = document.createElement("td");
            distanceCell.setAttribute("scope", "col");
            let paceCell = document.createElement("td");
            paceCell.setAttribute("scope", "col");
            if (i > 0) {
                let time = orienteerData[2][i];
                if (time > 0) {
                    let orienteerDistance = orienteerData[3][i]
                    timeCell.innerHTML = convertTimeToString(time);
                    distanceCell.innerHTML = deltaDistanceCalculator(orienteerDistance, distances[i]);
                    paceCell.innerHTML = computePace(time, distances[i]) + " / " + computeSpeed(time, distances[i]);
                    orienteerTotalDistance += orienteerDistance;
                    orienteerTotalTime += time;
                } else {
                    timeCell.innerHTML = "--:--";
                    distanceCell.innerHTML = "-";
                    paceCell.innerHTML = "--:--";
                }
            }
            rows[index].appendChild(timeCell);
            rows[index].appendChild(distanceCell);
            rows[index].appendChild(paceCell);
        }

        let totalTimeCell = document.createElement("th");
        totalTimeCell.style.background = "lightgray";
        totalTimeCell.setAttribute("scope", "col");
        let totalDistanceCell = document.createElement("th");
        totalDistanceCell.setAttribute("scope", "col");
        let speedMeanCell = document.createElement("th");
        speedMeanCell.setAttribute("scope", "col");

        totalTimeCell.innerHTML = convertTimeToString(orienteerTotalTime);
        totalDistanceCell.innerHTML = deltaDistanceCalculator(orienteerTotalDistance, totalTheoreticalDistance);
        speedMeanCell.innerHTML = computePace(orienteerTotalTime, totalTheoreticalDistance).toString()
            + " / "
            + computeSpeed(orienteerTotalTime, totalTheoreticalDistance).toString();

        totalsRow.appendChild(totalTimeCell);
        totalsRow.appendChild(totalDistanceCell);
        totalsRow.appendChild(speedMeanCell);
    });

    tableElement.appendChild(head);
    tableElement.appendChild(body);

    return tableElement;
}
