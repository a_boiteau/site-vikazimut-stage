function addTrack(event, map, gpxTrack) {
    let color = colorPicker.removeColor();
    let gpx = new L.GPX(gpxTrack, {
        async: true,
        marker_options: {
            startIconUrl: '/public/leaflet/images/pin-icon-start.png',
            endIconUrl: null,
            shadowUrl: null
        },
        polyline_options: {
            color: color,
            weight: 6,
            lineCap: 'round'
        }
    });
    gpx.addTo(map);
    event.target.style.borderColor = color;
    return [gpx, color];
}

function removeTrack(event, map, gpx) {
    let color = event.target.style.borderColor;
    colorPicker.addColor(color);
    gpx.clearLayers();
    event.target.style.borderColor = "white";
}

function clearTracks(){
    for (let traces of Object.values(gpxDictionary)){
        let gpx = traces[0];
        let color = traces[1];
        colorPicker.addColor(color);
        gpx.clearLayers();
    }
    document.querySelectorAll("button").forEach( el => {
        el.style.borderColor = "white";
    });
    gpxDictionary = {};
}