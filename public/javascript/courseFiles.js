function addDownloadButton(event, pseudo, track, div) {
    let button = document.createElement("button");
    button.setAttribute('class', 'btn btn-primary');
    button.innerHTML = " " + pseudo + ".gpx";
    button.style.cssText = "display: block; margin-top: 5px; margin-bottom: 5px";
    let icon = document.createElement("span");
    icon.className = "fa fa-download";
    button.prepend(icon);
    div.appendChild(button);
    let handler1 = function () {
        downloadFile(pseudo + ".gpx", track);
    };
    button.addEventListener("click", handler1);
    return button;
}

function removeDownloadButton(div, button) {
    div.removeChild(button);
}

function clearDownloadButton(div) {
    while (div.hasChildNodes()) {
        div.firstChild.remove();
    }
}

function downloadFile(filename, contents) {
    const localHeader = "<gpx version=\"1.0\">"
    if (contents.startsWith(localHeader)) {
        contents = contents.substring(localHeader.length);
        let header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><gpx version=\"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"Vikazimut\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">";
        contents = header + contents;
    }
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(contents));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}
