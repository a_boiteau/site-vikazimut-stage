# Site Vikazimut

## Le projet Vikazimut
Vikazimut est un projet d'étudiants en informatique de l'[ENSICAEN](https://www.ensicaen.fr)
et de l'[université de Caen Normandie](https://uniform.unicaen.fr/catalogue/formation/licences/5744-licence-informatique).

Le projet répond à une demande de l'association [Vik'Azim](https://vikazim.fr)
visant la réalisation d'une application Android pour la pratique de la course d'orientation.

L’application mobile Vikazimut a pour objectif de faciliter la pratique de la course
d’orientation. Elle remplace la carte papier, la boussole et le poinçon de validation
des points de contrôle.

Un parcours d'orientation consiste en une suite de points de contrôle matérialisés sur le
terrain par une balise type fédération internationale de course d’orientation (IOF) contenant
en plus un code QR et un tag NFC.

L’orienteur utilise l’application pour se repérer à partir de la carte et valider son passage
aux points de contrôle avec le lecteur de code QR ou le lecteur NFC si le parcours est équipé de
balises physiques ou par détection automatique de la position GPS.

L’application affiche en fin du parcours des statistiques sur la réalisation du parcours :
le temps total, le temps intermédiaire entre chaque balise et le tracé du parcours réalisé
sur la carte.

L’application se présente sous deux modes : un mode course où l’orienteur n’est pas aidé pour
sa localisation et un mode promenade où l’orienteur est positionné sur la carte en temps réel.

## Le site

Le présent projet correspond au site web de l'application [Vikazimut](https://vikazimut.vikazim.fr).

##  L'équipe Vikazimut ([Université de Caen Normandie](https://www.unicaen.fr/) - ([spécialité informatique](https://uniform.unicaen.fr/catalogue/formation/licences/5744-licence-informatique))

### 2019/2020
- Martin FÉAUX DE LA CROIX 

### 2020/2021
- Antoine BOITEAU
- Suliac LAVENANT
