# Déploiement du site sur le serveur hôte

## Sur la machine de développement

```shell script
cd site-vikazimut
./deploy.sh
```

Le fichier deploy.sh contient une commande rsync pour ne copier que les fichiers modifiés :
```shell script
rsync -av ./  u51382475@home468677337.1and1-data.host:~/appli --exclude-from=.gitignore --include=public/build --include=vendor --exclude "uml" --exclude "bin" --exclude=".*" --exclude "*~"
```

## Sur le serveur hôte

```shell script
ssh u51382475@home468677337.1and1-data.host
cd appli
php7.4-cli bin/console cache:clear --env=prod --no-debug
```

# Déploiement complet (première fois)

Une version du site est archivée dans le dossier "save" avec noteament les fichiers spécifiques de la version de la mise en production.
  ```
  - .env
  - .env.local.php
  ```

# Mettre à jour la version de synfony sur le serveur

Modifier le fichier `composer.json` avec les bonnes versions, puis lancer les commandes :

```shell script
cd projet
php7.4-cli composer.phar update --no-dev
```
